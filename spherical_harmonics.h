#include "../Library/settings.h"
#include "../Library/basic.h"

#include <complex.h>

void transformToSpherical(const real* origin, const real* point, real* res);
complex double evalSpherical(int l, int m, real theta, real phi);
void setupCoefficients(int maxDegree);
void deleteCoefficients(int maxDegree);
real prodFromTo(int n, int m);
field powCN(field base, int exponent);

// real evalFromCoeffs(real* coeffs, real point, int degree);
// real* derive(real* coeffs, int degree, int order);
// real** calculateLegendreCoeff(int maxDegree);