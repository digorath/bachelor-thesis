#include "settings.h"
#include "h2matrix.h"
#include "spatialcluster.h"

typedef struct _sList sList;

/** @brief List of interaction matrices corresponding to relative location of clusters */
struct _sList {
  pamatrix S;
  uint directionalIdx;
  sList *next;
};

/** @brief Data required to approximate a kernel matrix. */
typedef struct _potentialmatrix potentialmatrix;

/** @brief Pointer to a @ref kernelmatrix object. */
typedef potentialmatrix *ppotentialmatrix;

/** @brief Pointer to a constant @ref kernelmatrix object. */
typedef const potentialmatrix *pcpotentialmatrix;

/** @brief Pointer to a potential function. */
typedef real (*potential) (pcreal xx, pcreal yy);

/** @brief Representation of a kernel matrix an its approximation.
 *
 *  A kernel matrix is a matrix with entries of the form
 *  @f$g_{ij} = k(x_i,x_j)@f$, where @f$k@f$ is a kernel functions
 *  and @f$(x_i)_{i\in\Idx}@f$ are points in a suitable space.
 *
 *  If the kernel functions is locally smooth, it can be approximated
 *  by interpolation, and this gives rise to blockwise low-rank
 *  approximations. */
struct _potentialmatrix {
  /** @brief Spatial dimension. */
  uint dim;

  /** @brief Kernel function. */
  potential g;

  /** @brief Number of points. */
  uint points;

  /** @brief Coordinates of points. */
  real **x;

  /** @brief Order of approximation. */
  int nu;
};

pamatrix lookUpS(uint directionalIndex, sList *l);
void initPotentialModule(int nu);
void unInitPotentialModule();
void fillV_col_potentialmatrix(pcspatialcluster tc, pcpotentialmatrix pm, pamatrix V);
void fillE_col_potentialmatrix(pcspatialcluster sc, pcspatialcluster fc, pcpotentialmatrix pm, pamatrix E);
void fillS_potentialmatrix(pcspatialcluster rc, pcspatialcluster cc, pcpotentialmatrix pm, pamatrix S);
void fillE_row_potentialmatrix(pcspatialcluster sc, pcspatialcluster fc, pcpotentialmatrix pm, pamatrix E);
void fillV_row_potentialmatrix(pcspatialcluster tc, pcpotentialmatrix pm, pamatrix V);
void fillN_potentialmatrix(const uint *ridx, const uint *cidx, pcpotentialmatrix pm, pamatrix N);

void fill_col_clusterbasis_potentialmatrix(pcpotentialmatrix pm, pclusterbasis cb);
void fill_row_clusterbasis_potentialmatrix(pcpotentialmatrix pm, pclusterbasis cb);

void fill_h2matrix_potentialmatrix(pcpotentialmatrix pm, ph2matrix G);

void del_potentialmatrix(ppotentialmatrix pm);