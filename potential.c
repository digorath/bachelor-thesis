
#include "spatialgeometry.h"

#include "potential.h"
#include "parameters.h"
#include "basic.h"
#include "spherical_harmonics.h"

#include <complex.h>
#include <stdio.h>
#include <stdlib.h>

real** translationAngles;
real* a;
pamatrix* rowSmartE;
pamatrix* colSmartE;
sList* smartS;
pfield* smartHarmonics;
static real A(int n, int m);
static void setupA(int nu);
static void setupTranslationAngles();
static void setupSmartSphericals(int maxDegree);
static void setupColSmartE(int maxDegree);
static void setupRowSmartE(int maxDegree);
static void freeDirectionalIndices(pspatialcluster sroot);
static void freeS();
static void freeSphericals(int maxDegree);
static void freeTransfer();
static void freeTranslationAngles();
static uint relativeDirection(pcspatialcluster sc, pcspatialcluster tc);
static short sonDirection(pcspatialcluster fc, pcspatialcluster sc);
static int combinedIndex(int l, int m);
static void setupDirectionalIndices(pspatialcluster sroot);
static pamatrix getSmartS(pcpotentialmatrix pm, pcspatialcluster cc, pcspatialcluster rc);
static pamatrix setupNewS(pcspatialcluster cc, pcspatialcluster rc, uint directionalIndex, pcpotentialmatrix pm);


static real A(int n, int m) {
  real res,denom;
  res = powCN(-1.0, n);
  denom = sqrt(prodFromTo(1, n-m) * prodFromTo(1, n+m));
  res = res/denom;
  return res;
}

static void setupTranslationAngles(){
  translationAngles = allocmem(6 * sizeof(real*));
  for(int l = 0; l<6; l++) {
    translationAngles[l] = allocmem(2*sizeof(real));
  }
  // away from x-Axis
  translationAngles[0][0] = 0.5 * M_PI;
  translationAngles[0][1] = M_PI;
  // away from y-Axis
  translationAngles[1][0] = 0.5 * M_PI;
  translationAngles[1][1] = -0.5 * M_PI;
  // away from z-Axis
  translationAngles[2][0] = M_PI;
  translationAngles[2][1] = 0.0;
  // towards x-Axis
  translationAngles[3][0] = 0.5 * M_PI;
  translationAngles[3][1] = 0.0;
  // towards of y-Axis
  translationAngles[4][0] = 0.5 * M_PI;
  translationAngles[4][1] = 0.5 * M_PI;
  // towards of z-Axis
  translationAngles[5][0] = 0.0;
  translationAngles[5][1] = 0.0;


}

static void setupA(int nu) {
  a = allocmem((combinedIndex(2*nu,2*nu)+1) * sizeof(real));
  for(int n = 0; n <= 2*nu; n++){
    for(int m = -n; m <= n; m++) {
      a[combinedIndex(n,m)] = A(n,m);
    }
  }
}

static void setupSmartSphericals(int maxDegree) {
  smartHarmonics = allocmem(6 * sizeof(pfield));
  for(int l = 0; l<6; l++) {
    smartHarmonics[l] = allocmem((combinedIndex(maxDegree,maxDegree) + 1) * sizeof(field));
    for(int j = 0; j<=maxDegree; j++) {
      for(int k = -j; k <= j; k++) {
        smartHarmonics[l][combinedIndex(j,k)] = evalSpherical(j,k,translationAngles[l][0],translationAngles[l][1]);
      }
    }
  }
  
}

static void setupColSmartE(int nu) {
  int rank = combinedIndex(nu,nu)+1;
  int j,k,l,m,n,rowIndex,colIndex;
  pfield entries;

  colSmartE = allocmem(6 * sizeof(pamatrix));
  for(l = 0; l<6; l++) {
    colSmartE[l] = new_amatrix(rank,rank);
    clear_amatrix(colSmartE[l]);
    entries = colSmartE[l]->a;

    for(j = 0; j <= nu; j ++) {
      for(k = -j; k <= j; k++) {
        rowIndex = combinedIndex(j,k);
        for(n = 0; n <= j; n++) {
          for (m = -n; m <= n; m++) {
            if(abs(k-m) <= j - n) {
              colIndex = combinedIndex(j-n,k-m);
              entries[colIndex + rowIndex * rank] = conj(powCN(I, abs(k)-abs(m)-abs(k-m)) * a[combinedIndex(n,m)] * a[combinedIndex(j-n,k-m)]/a[combinedIndex(j,k)] * smartHarmonics[l][combinedIndex(n, -m)]);
            }
          }
        }
      }
    }
  }
}

static void setupRowSmartE(int nu) {
  int rank = combinedIndex(nu,nu)+1;
  int j,k,l,m,n,rowIndex,colIndex;
  pfield entries;

  rowSmartE = allocmem(6 * sizeof(pamatrix));
  for(l = 0; l<6; l++) {
    rowSmartE[l] = new_amatrix(rank,rank);
    clear_amatrix(rowSmartE[l]);
    entries = rowSmartE[l]->a;

    for(j = 0; j <= nu; j ++) {
      for(k = -j; k <= j; k++) {
        rowIndex = combinedIndex(j,k);
        for(n = j; n <= nu; n++) {
          for (m = -n; m <= n; m++) {
            if(abs(m - k) <= n - j) {
              colIndex = combinedIndex(n,m); 
              entries[rowIndex+ colIndex * rank] = powCN(-1.0, n+j) *powCN(I, abs(m)-abs(m-k)-abs(k)) * a[combinedIndex(n-j,m-k)]*a[combinedIndex(j,k)]/a[combinedIndex(n, m)] * smartHarmonics[l][combinedIndex(n - j, m - k)]; 
            }
          }
        }
      }
    }
  }
}

static void freeDirectionalIndices(pspatialcluster sroot) {
  free(sroot->dir);
  for(int k = 0; k<sroot->sons; k++) {
    freeDirectionalIndices(sroot->son[k]);
  }
}

static void freeS(){
  sList* cur = smartS;
  sList* next = NULL;
  while (cur) {
    if(cur->S != NULL) {
      del_amatrix(cur->S);
    }
      next = cur->next;
    free(cur);
    cur = next;
  }
}

static void freeSphericals(int maxDegree) {
  for(int k = 0; k<5;k++) {
    free(smartHarmonics[k]);
  }
  free(smartHarmonics);
}

static void freeTransfer() {
  for(int k = 0; k<5; k++) {
    del_amatrix(colSmartE[k]);
    del_amatrix(rowSmartE[k]);
  }
  free(colSmartE);
  free(rowSmartE);
}

static void freeTranslationAngles() {
  for(int l = 0; l<6; l++) {
    free(translationAngles[l]);
  }
  free(translationAngles);
}

// calculates an index for the relative prosition of two spatialclusters
static uint relativeDirection(pcspatialcluster sc, pcspatialcluster tc) {
  uint diffIdx = 0;
  directionalIndex *sdir;
  directionalIndex *tdir;
  assert(sc->dir);
  assert(tc->dir);
  sdir = sc->dir;
  tdir = tc->dir;
  while(sdir) {
    assert(tdir);
    diffIdx = 3*diffIdx;
    // due to the implementation of the KIPS library the direction is either the same or exactly opposite.
    if(sdir->idx > tdir->idx) diffIdx = diffIdx + 1;
    if(sdir->idx < tdir->idx) diffIdx = diffIdx + 2;

    // go to fathers
    sdir = sdir->fatherDir;
    tdir = tdir->fatherDir;
  }
  return diffIdx;
}

// checks in which direction the father and son are located relative to eachother
static short sonDirection(pcspatialcluster fc, pcspatialcluster sc) {
  if(fabs(fc->bmin[0]- sc->bmin[0]) > 0.0) return 0;
  if(fabs(fc->bmin[1]- sc->bmin[1]) > 0.0) return 1;
  if(fabs(fc->bmin[2]- sc->bmin[2]) > 0.0) return 2;
  if(fabs(fc->bmax[0]- sc->bmax[0]) > 0.0) return 3;
  if(fabs(fc->bmax[1]- sc->bmax[1]) > 0.0) return 4;
  if(fabs(fc->bmax[2]- sc->bmax[2]) > 0.0) return 5;
  return 6;
}

// combines the two indices of spherical harmonics into one
static int combinedIndex(int l, int m) {
  return l*l + l + m;
}

// calculates and stores directional indices for the clustertree
static void setupDirectionalIndices(pspatialcluster fc) {
  pspatialcluster son;
  if(fc->sons > 0) {
    for(int k = 0; k < fc->sons; k++){
      son = fc->son[k];
      son->dir = (directionalIndex*) allocmem(sizeof(directionalIndex));
      if(fc->dir) {
        son->dir->fatherDir = fc->dir;
      } else {
        son->dir->fatherDir = NULL;
      }
      son->dir->idx = sonDirection(fc, son);
      setupDirectionalIndices(son);
    }
  }
}

static pamatrix setupNewS(pcspatialcluster cc, pcspatialcluster rc, uint directionalIndex, pcpotentialmatrix pm) {
  int rowIndex, colIndex, j, k, m, n;
  int nu = pm->nu;
  int rank = combinedIndex(nu,nu)+1;
  sList* listIterator;
  pamatrix res;
  pfield entries;
  real* sourceCenter;
  real* targetCenter;
  real rho,alpha,beta;
  real* sphericalCoords;
  sourceCenter = allocmem(3*sizeof(real));
  targetCenter = allocmem(3*sizeof(real));
  sphericalCoords = allocmem(3*sizeof(real));

  for(int idx = 0; idx < 3; idx++) {
    sourceCenter[idx] = (cc->bmax[idx] + cc->bmin[idx])/2.0;
    targetCenter[idx] = (rc->bmax[idx] + rc->bmin[idx])/2.0;
  }

  transformToSpherical(targetCenter, sourceCenter, sphericalCoords);
  rho   = sphericalCoords[0];
  alpha = sphericalCoords[1];
  beta  = sphericalCoords[2];
  res = new_amatrix(rank, rank);
  clear_amatrix(res);
  entries = res->a;
  for(j = 0; j <= nu; j ++) {
    for(k = -j; k <= j; k++) {
      rowIndex = combinedIndex(j,k);
      for(n = 0; n <= nu; n++) {
        for (m = -n; m <= n; m++) {
            colIndex = combinedIndex(n,m); 
            entries[rowIndex+ colIndex * rank] = powCN(-1.0, n) * powCN(I, abs(k-m)-abs(k)-abs(m)) * a[combinedIndex(j,k)] * a[combinedIndex(n,m)]/a[combinedIndex(j+n,m-k)] * powCN(rho,-(j+n+1)) * evalSpherical(j+n, m-k, alpha, beta); 
        }
      }
    }
  }
  listIterator = smartS;
  while(listIterator->next) {
    listIterator = listIterator->next;
  }
  listIterator->next = (sList*) allocmem(sizeof(sList));
  listIterator->next->directionalIdx = directionalIndex;
  listIterator->next->S = res;
  listIterator->next->next = NULL;
  return res;
}

// gets a Transfermatrix corresponding to the direction. if it does not exist yet, it will create a new one
static pamatrix getSmartS(pcpotentialmatrix pm, pcspatialcluster cc, pcspatialcluster rc) {
  uint directionalIndex = relativeDirection(cc, rc);
  pamatrix res = lookUpS(directionalIndex, smartS);
  if(!res) res = setupNewS(cc, rc, directionalIndex, pm);
  return res;
}

pamatrix lookUpS(uint directionalIndex, sList *l) {
  if(l->directionalIdx == directionalIndex) return l->S;
  if(l->next) return lookUpS(directionalIndex, l->next);
  return NULL;
}

// sets up all the necessary variables including coefficients for spherical harmonics and smart transfer matrices
void initPotentialModule(int nu){
  setupA(nu);
  setupTranslationAngles();
  setupCoefficients(2 * nu);
  setupSmartSphericals(2 * nu);
  setupColSmartE(nu);
  setupRowSmartE(nu);
  smartS = (sList*) allocmem(sizeof(sList));
  smartS->next=NULL;
  smartS->directionalIdx=0;
}

// frees all memory, which is allocated in helper variables
void unInitPotentialModule(ppotentialmatrix pm) {
  free(a);
  freeTranslationAngles();
  deleteCoefficients(pm->nu * 2);
  freeSphericals(pm->nu * 2);
  freeTransfer();
  // freeS();
  del_potentialmatrix (pm);
}

// Fills the leaf cluster matrix with coefficients of the corresponding multipole expansion of the potential induced by particles in hte cluster
void fillV_col_potentialmatrix(pcspatialcluster tc, pcpotentialmatrix pm, pamatrix V) {
  uint dim = pm->dim;
  int nu = pm->nu;
  const real **x = (const real **) pm->x;
  const uint *idx = tc->idx;
  int l, ld;

  real* center = allocmem(3*sizeof(real));
  for(int n = 0; n < 3; n++){
    center[n] = (tc->bmax[n] + tc->bmin[n])/2.0;
  } 
  real* sphericalCoords = allocmem(3*sizeof(real));
  assert(tc->dim == dim);
  
  clear_amatrix(V);
  pfield Va = V->a;
  ld = V->ld;
  for(int i = 0; i < V->rows; i++) {
    transformToSpherical(center, x[idx[i]], sphericalCoords);
    for(int n = 0; n <= nu; n++) {
      for(int m = -n; m <= n; m++) {
        l = combinedIndex(n,m);
        Va[i + ld * l] = conj(powCN(sphericalCoords[0], n) * evalSpherical(n, -m, sphericalCoords[1], sphericalCoords[2]));
      }
    }
  }
}

// Fills the transfermatrices with the coefficients for translating from sonCluster sc to fatherCluster fc
void fillE_col_potentialmatrix(pcspatialcluster sc, pcspatialcluster fc, pcpotentialmatrix pm, pamatrix E) {
  uint dim = pm->dim;
  assert(sc->dim == dim);
  assert(fc->dim == dim);
  uint ld = E->ld;
  int nu = pm->nu;

  int j,k,l,m,n,rowIndex, colIndex;

  real* sonCenter;
  real* fatherCenter;
  real rho;

  sonCenter = allocmem(3*sizeof(real));
  fatherCenter = allocmem(3*sizeof(real));
  for(int idx = 0; idx < 3; idx++) {
    sonCenter[idx] = (sc->bmax[idx] + sc->bmin[idx])/2.0;
    fatherCenter[idx] = (fc->bmax[idx] + fc->bmin[idx])/2.0;
  }

  rho = 0.0;
  for(int idx = 0; idx < 3; idx++) {
    rho += REAL_SQR(fatherCenter[idx] - sonCenter[idx]);
  }
  rho = sqrt(rho);

  // shift direction index to accomodate interchanging source and target roles between father and son
  l = (sc->dir->idx + 3)%6;

  clear_amatrix(E);
  pfield Ea = E->a;

  for(j = 0; j <= nu; j ++) {
    for(k = -j; k <= j; k++) {
      rowIndex = combinedIndex(j,k);
      for(n = 0; n <= j; n++) {
        for (m = -n; m <= n; m++) {
          if(abs(k-m) <= j - n) {
            colIndex = combinedIndex(j-n,k-m);
            Ea[colIndex + rowIndex * ld] = colSmartE[l]->a[colIndex + rowIndex * ld] * powCN(rho,n);
          }
        }
      }
    }
  }
}

// Fills the interaction matrices with coefficients for converting the multipole expansion of the columnCluster cc to a local expansion of the rowCluster rc
void fillS_potentialmatrix(pcspatialcluster rc, pcspatialcluster cc, pcpotentialmatrix pm, pamatrix S) {
  copy_amatrix(false, getSmartS(pm, cc, rc), S);
}

// Fills the transfer matrices with the coefficients for translating from fatherCluster fc to sonCluster sc
void fillE_row_potentialmatrix(pcspatialcluster sc, pcspatialcluster fc, pcpotentialmatrix pm, pamatrix E) {
  uint dim = pm->dim;
  uint ld = E->ld;
  int nu = pm->nu;

  assert(sc->dim == dim);
  assert(fc->dim == dim);

  int j,k,l,m,n,rowIndex, colIndex;

  real* sonCenter;
  real* fatherCenter;
  real rho;
  sonCenter = allocmem(3*sizeof(real));
  fatherCenter = allocmem(3*sizeof(real));

  for(int idx = 0; idx < 3; idx++) {
    sonCenter[idx] = (sc->bmax[idx] + sc->bmin[idx])/2.0;
    fatherCenter[idx] = (fc->bmax[idx] + fc->bmin[idx])/2.0;
  }
  rho = 0.0;
  for(int idx = 0; idx < 3; idx++) {
    rho += REAL_SQR(fatherCenter[idx] - sonCenter[idx]);
  }
  rho = sqrt(rho);

  l = sc->dir->idx;
  clear_amatrix(E);
  pfield Ea = E->a;
  for(j = 0; j <= nu; j ++) {
    for(k = -j; k <= j; k++) {
      rowIndex = combinedIndex(j,k);
      for(n = j; n <= nu; n++) {
        for (m = -n; m <= n; m++) {
          if(abs(m - k) <= n - j) {
            colIndex = combinedIndex(n,m); 
            Ea[rowIndex+ colIndex * ld] = rowSmartE[l]->a[rowIndex + colIndex * ld] * powCN(rho, n - j); 
          }
        }
      }
    }
  }
}

// Fills the leaf matrix of the row cluster with the matrix corresponding to evaluating a local expansion in particle positions of the cluster
void fillV_row_potentialmatrix(pcspatialcluster tc, pcpotentialmatrix pm, pamatrix V) {
  uint dim = pm->dim;
  int nu = pm->nu;
  const real **x = (const real **) pm->x;
  const uint *idx = tc->idx;
  int l, ld;

  real* center = allocmem(3*sizeof(real));
  for(int n = 0; n < 3; n++){
    center[n] = (tc->bmax[n] + tc->bmin[n])/2.0;
  } 
  real* sphericalCoords = allocmem(3*sizeof(real));
  assert(tc->dim == dim);

  clear_amatrix(V);
  pfield Va = V->a;
  ld = V->ld;
  for(int n = 0; n <= nu; n++) {
    for(int m = -n; m <= n; m++) {
      for(int i = 0; i < V->rows; i++) {
        transformToSpherical(center, x[idx[i]], sphericalCoords);
        l = combinedIndex(n,m);
        assert(i < V->rows);
        assert(l < V->cols);
        Va[i + ld * l] = powCN(sphericalCoords[0], n) * evalSpherical(n, m, sphericalCoords[1], sphericalCoords[2]);
      }
    }
  }
}

// Fills the dense matrix for the potential evaluation of the near field terms
void fillN_potentialmatrix(const uint *ridx, const uint *cidx, pcpotentialmatrix pm, pamatrix N) {
  const real **x = (const real **) pm->x;
  uint rows = N->rows;
  uint cols = N->cols;
  pfield Na = N->a;
  uint ldN = N->ld;
  uint i, j, ii, jj;
  potential g = pm->g;

  clear_amatrix(N);

  if(ridx) {
    if(cidx) {
      for(j=0; j<cols; j++) {
        jj = cidx[j];

        for(i=0; i<rows; i++) {
          ii = ridx[i];

          Na[i+j*ldN] = g(x[ii], x[jj]);
        }
      }
    }
    else {
      assert(cols <= pm->points);
      for(j=0; j<cols; j++) {
        for(i=0; i<rows; i++) {
          ii = ridx[i];

          Na[i+j*ldN] = g(x[ii], x[j]);
        }
      }
    }
  }
  else {
    assert(rows <= pm->points);

    if(cidx) {
      for(j=0; j<cols; j++) {
        jj = cidx[j];

        for(i=0; i<rows; i++)
          Na[i+j*ldN] = g(x[i], x[jj]);
      }
    }
    else {
      assert(cols <= pm->points);
      for(j=0; j<cols; j++)
	      for(i=0; i<rows; i++)
	        Na[i+j*ldN] = g(x[i], x[j]);
    }
  }
}

// Fills the clusterbasis matrices corresponding to the rows.
void fill_col_clusterbasis_potentialmatrix(pcpotentialmatrix pm, pclusterbasis cb) {
  uint i, rank;
  int nu = pm->nu;

  rank = combinedIndex(nu,nu)+1;

  for(i=0; i<cb->sons; i++) fill_col_clusterbasis_potentialmatrix(pm, cb->son[i]);
  setrank_clusterbasis(rank, cb);

  if(cb->sons > 0) {
    for(i=0; i<cb->sons; i++)
      fillE_col_potentialmatrix(cb->son[i]->t, cb->t, pm, &cb->son[i]->E);
  }
  else {
    fillV_col_potentialmatrix(cb->t, pm, &cb->V);
  }
  update_clusterbasis(cb);
}

// Fills the clusterbasis matrices corresponding to the columns.
void fill_row_clusterbasis_potentialmatrix(pcpotentialmatrix pm, pclusterbasis cb) {
  uint i,rank;
  int nu = pm->nu;

  rank = combinedIndex(nu,nu)+1;

  for(i=0; i<cb->sons; i++) fill_row_clusterbasis_potentialmatrix(pm, cb->son[i]);
  setrank_clusterbasis(rank, cb);

  if(cb->sons > 0) {
    for(i=0; i<cb->sons; i++) {
      fillE_row_potentialmatrix(cb->son[i]->t, cb->t, pm, &cb->son[i]->E);
    }
  }
  else {
    fillV_row_potentialmatrix(cb->t, pm, &cb->V);
  }
  update_clusterbasis(cb);
}

// Fills the h2 matrix using the parameters given by the potentialmatrix
void fill_h2matrix_potentialmatrix(pcpotentialmatrix pm, ph2matrix G) {
  uint rsons, csons;
  uint i, j;

  if(G->son) {
    rsons = G->rsons;
    csons = G->csons;

    for(j=0; j<csons; j++){
      for(i=0; i<rsons; i++){
        fill_h2matrix_potentialmatrix(pm, G->son[i+j*rsons]);
      }
    }
  }
  else if(G->u) 
    fillS_potentialmatrix(G->rb->t, G->cb->t, pm, &G->u->S);
  else {
    assert(G->f);
    fillN_potentialmatrix(G->rb->t->idx, G->cb->t->idx, pm, G->f);
  }
}

// calculates the potential induced by one particle with unit charge at the position of one point in the other point
static real potential_3d(const real *xx, const real *yy) {
  real norm2;

  norm2 = REAL_SQR(xx[0] - yy[0]) + REAL_SQR(xx[1] - yy[1]) + REAL_SQR(xx[2] - yy[2]);

  return (norm2 == 0.0 ? 0.0 : 1.0 / REAL_SQRT(norm2));
}

// initializes a potentialmatrix allocating memory and the parameters of the dimension and the order nu of the multipole and local expansions
ppotentialmatrix new_potentialmatrix(uint dim, uint points, int nu) {
  ppotentialmatrix pm;
  real *x0;
  uint i;

  /* Initialize basic structure */
  pm = (ppotentialmatrix) allocmem(sizeof(potentialmatrix));
  pm->dim = dim;
  pm->points = points;

  /* Set order of approximation */
  pm->nu = nu;

  /* Empty kernel callback function */
  pm->g = 0;

  /* Initialize arrays for point coordinates */
  pm->x = (real **) allocmem(sizeof(real *) * points);
  pm->x[0] = x0 = allocreal(points * dim);
  for(i=1; i<points; i++) {
    x0 += dim;
    pm->x[i] = x0;
  }

  return pm;
}

void del_potentialmatrix(ppotentialmatrix pm) {
  freemem(pm->x[0]);
  freemem(pm->x);
  freemem(pm);
}

// test routine for testing the column transfer matrices
void testColTransfer(pclusterbasis cc, ppotentialmatrix pm) {
  pamatrix V1, V2, E1, E2, V_dad, res1, res2, res_combined;
  uint dad_rows;
  pspatialcluster cluster_copy;

  E1 = &cc->son[0]->E;
  E2 = &cc->son[1]->E;
  V1 = &cc->son[0]->V;
  V2 = &cc->son[1]->V;
  
  dad_rows = V1->rows + V2->rows;

  res_combined = new_amatrix(E1->cols, dad_rows);
  clear_amatrix(res_combined);

  res1 = new_sub_amatrix(res_combined, E1->rows, 0, V1->rows, 0);
  res2 = new_sub_amatrix(res_combined, E2->rows, 0, V2->rows, V1->rows);

  // calculate E * V
  addmul_amatrix(1.0, true, E1, true, V1, res1);
  addmul_amatrix(1.0, true, E2, true, V2, res2);
  
  // create V_dad
  V_dad = new_amatrix(dad_rows, V1->cols);
  clear_amatrix(V_dad);
  
  uint* newIdx = allocmem(dad_rows * sizeof(uint));
  for(int i = 0; i < V1->rows; i++) {
    newIdx[i] = cc->son[0]->t->idx[i];
  }
  for(int i = 0; i < V2->rows; i++) {
    newIdx[i+V1->rows] = cc->son[1]->t->idx[i];
  }
  cluster_copy = new_spatialcluster(cc->t->dim, cc->t->bmin, cc->t->bmax, cc->t->sons, 2);
  cluster_copy->idx = newIdx;

  fillV_col_potentialmatrix(cluster_copy, pm, V_dad);

  // compare
  add_amatrix(-1.0, true, V_dad, res_combined);
  printf("norm2 col transfer error: %f\n", norm2_amatrix(res_combined)/norm2_amatrix(V_dad));
  del_amatrix(V_dad);
  del_amatrix(res_combined);
  freemem(cluster_copy->bmin);
  freemem(cluster_copy->bmax);
  freemem(cluster_copy);
}

// test routine for testing the row transfer matrices
void testRowTransfer(pclusterbasis rc, ppotentialmatrix pm) {
  pamatrix V1, V2, E1, E2, V_dad, res1, res2, res_combined;
  uint dad_rows;
  pspatialcluster cluster_copy;

  E1 = &rc->son[0]->E;
  E2 = &rc->son[1]->E;
  V1 = &rc->son[0]->V;
  V2 = &rc->son[1]->V;
  
  dad_rows = V1->rows + V2->rows;

  res_combined = new_amatrix(dad_rows, E1->cols);
  clear_amatrix(res_combined);

  res1 = new_sub_amatrix(res_combined, V1->rows, 0, E1->cols, 0);
  res2 = new_sub_amatrix(res_combined, V2->rows, V1->rows, E2->rows, 0);

  // calculate V * E
  addmul_amatrix(1.0, false, V1, false, E1, res1);
  addmul_amatrix(1.0, false, V2, false, E2, res2);
  
  // create V_dad
  V_dad = new_amatrix(dad_rows, E1->cols);
  clear_amatrix(V_dad);
  
  uint* newIdx = allocmem(dad_rows * sizeof(uint));
  for(int i = 0; i < V1->rows; i++) {
    newIdx[i] = rc->son[0]->t->idx[i];
  }
  for(int i = 0; i < V2->rows; i++) {
    newIdx[i+V1->rows] = rc->son[1]->t->idx[i];
  }
  cluster_copy = new_spatialcluster(rc->t->dim, rc->t->bmin, rc->t->bmax, rc->t->sons, 2);
  cluster_copy->idx = newIdx;

  fillV_row_potentialmatrix(cluster_copy, pm, V_dad);

  // compare
  printf("norm2 row transfer error: %f\n", norm2diff_amatrix(res_combined, V_dad)/norm2_amatrix(V_dad));

  del_amatrix(V_dad);
  del_amatrix(res_combined);
  freemem(cluster_copy->bmin);
  freemem(cluster_copy->bmax);
  freemem(cluster_copy);
}

// test routine for testing the column leaf matrices
void testMultipole(pclusterbasis cc, ppotentialmatrix pm) {
  // get multipole coefficients
  pavector c, c_hat, coeffs;
  real *evalpoint, *center;
  pamatrix V;
  uint *idx = cc->son[0]->t->idx;
  field res, ref;
  real** x = pm->x;
  
  V = &cc->son[0]->V;
  c = new_avector(pm->points);
  random_avector(c);

  c_hat = new_avector(V->rows);
  for (int i = 0; i< V->rows; i++) {
    c_hat->v[i] = c->v[idx[i]];
  }
  coeffs = new_avector(V->cols);
  clear_avector(coeffs);
  mvm_amatrix(1.0, true, V, c_hat, coeffs);

  evalpoint = allocmem(3*sizeof(real));
  evalpoint[0] = 5.0;
  evalpoint[1] = 5.0;
  evalpoint[2] = 5.0;
  // calculate reference value
  ref = 0;
  for(int i = 0; i < V->rows; i++) {
    ref += c->v[idx[i]] * pm->g(x[idx[i]], evalpoint);
  }
  // evaluate multipole expansion
  center = allocmem(3*sizeof(real));
  for(int i = 0; i < 3; i++) {
    center[i] = (cc->son[0]->t->bmax[i] + cc->son[0]->t->bmin[i])/2.0;
  }
  transformToSpherical(center, evalpoint, evalpoint);
  res = 0;
  for(int j = 0; j<pm->nu; j++) {
    for(int k = -j; k <= j; k++) {
      res = res + coeffs->v[combinedIndex(j,k)] * powCN(evalpoint[0],-(j+1)) * evalSpherical(j, k, evalpoint[1], evalpoint[2]);
    }
  }

  printf("res: %f + I * %f; ref: %f + I * %f\n", creal(res), cimag(res), creal(ref), cimag(ref));
}

int main(int argc, char **argv) {
  real eta, norm, norm2, error, maxdiam, t_setup, *bmin, *bmax;
  uint i, j, dim, points, nu, maxdepth;
  pspatialgeometry sg;
  pspatialcluster sroot;
  pblock broot;
  pclusterbasis ccb, rcb;
  ph2matrix Gh2;
  pamatrix G;
  pavector c,r1, r2;
  ppotentialmatrix pm;
  pstopwatch sw;
  size_t sz;
  field alpha;
  pclusterbasis testcluster, testcluster_tmp;
  real T_K = 0, T_ref = 0;
  size_t Mem_K = 0, Mem_ref = 0;

  init_kips (&argc, &argv);
  sw = new_stopwatch();
  
  dim = 3;
  maxdepth = askforint ("Maximal depth of spatial cluster tree?", "", 8);
  maxdiam = askforreal ("Maximal diameter of leaf clusters?", "", 0.01);
  points = askforint("Number of points?", "h2lib_kernelpoints", 12000);
  nu = askforint("Expansion order?", "h2lib_interorder", 6);
  eta = 0.5;

  initPotentialModule(nu);

  printf("Creating matrix object for %u points, order %u\n", points, nu);
  
  pm = new_potentialmatrix(3, points, nu);
  pm->g = &potential_3d;
  
  printf ("Creating bounding box, random points and random charge distribution\n");
  bmin = allocreal (dim);
  bmax = allocreal (dim);
  c = new_avector(points);
  random_avector(c);
  for (i=0; i<dim; i++) {
    bmin[i] = -1.0;
    bmax[i] = 1.0;
    for(j=0; j<points; j++) {
        pm->x[j][i] = FIELD_RAND();
    }
  }
  
  printf("Creating spatial cluster tree\n");
  sg = new_spatialgeometry (dim, bmin, bmax);
  sroot = init_spatialgeometry (maxdepth, maxdiam, sg);

  setupDirectionalIndices(sroot);

  printf ("%u spatial clusters\n", sroot->desc);
  
  printf ("Distributing random points to clusters\n");
  start_stopwatch (sw);
  initPoints_spatialgeometry (points, (const real **)pm->x, sg);
  t_setup = stop_stopwatch (sw);
  T_K += t_setup;
  printf ("%.6f seconds\n", t_setup);
  
  printf("Creating block tree\n");
  broot = buildh2std_block(sroot, sroot, eta);
  printf("%u blocks, depth %u\n",
		broot->desc, getdepth_block(broot));

  printf("Creating cluster basis\n");
  ccb = build_fromcluster_clusterbasis(sroot);
  rcb = build_fromcluster_clusterbasis(sroot);
  printf("Filling cluster basis\n");
  start_stopwatch(sw);
  fill_row_clusterbasis_potentialmatrix(pm, rcb);
  fill_col_clusterbasis_potentialmatrix(pm, ccb);
  t_setup = stop_stopwatch(sw);
  T_K += t_setup;

  sz = getsize_clusterbasis(ccb);
  sz += getsize_clusterbasis(rcb);
  Mem_K += sz;
  printf("  %.2f seconds\n" "  %.1f MB\n" "  %.1f KB/DoF\n", 
         t_setup, sz / 1048576.0, sz / 1024.0 / points);
  
  printf("testing col transfer\n");
  testcluster_tmp = ccb;
  while(testcluster_tmp->sons > 0) {
    testcluster = testcluster_tmp;
    testcluster_tmp = testcluster->son[0];
  }
  testColTransfer(testcluster, pm);
  
  printf("testing multipole \n");
  testMultipole(testcluster, pm);
  
  printf("testing row transfer\n");
  testcluster_tmp = rcb;
  while(testcluster_tmp->sons > 0) {
    testcluster = testcluster_tmp;
    testcluster_tmp = testcluster->son[0];
  }
  testRowTransfer(testcluster, pm);
  
  printf("Creating H^2-matrix\n");
  Gh2 = build_fromblock_h2matrix(broot, rcb, ccb);
  sz = getsize_h2matrix(Gh2);
  Mem_K += sz;
  printf("  %.1f MB\n" "  %.1f KB/DoF\n", sz / 1048576.0, sz / 1024.0 / points);

  printf("Filling H^2-matrix\n");
  start_stopwatch(sw);
  fill_h2matrix_potentialmatrix(pm, Gh2);
  t_setup = stop_stopwatch(sw);
  T_K += t_setup;
  printf("  %.2f seconds\n", t_setup);

  printf("Multiplying H^2-matrix with c\n");
  r1 = new_avector(points);
  clear_avector(r1);
  start_stopwatch(sw);
  mvm_h2matrix(1.0, false, Gh2, c, r1);
  t_setup = stop_stopwatch(sw);
  T_K += t_setup;
  printf("  %.2f seconds\n", t_setup);


  printf("Computing norm\n");
  norm = norm2_avector(r1);
  norm2 = norm;
  printf("  Euclidian norm %.3e\n", norm);

  printf("Computing reference vector\n");
  G = new_amatrix(points, points);
  start_stopwatch(sw);
  fillN_potentialmatrix(0, 0, pm, G);

  r2 = new_avector(points);
  clear_avector(r2);
  mvm_amatrix(1, false, G, c, r2);
  t_setup = stop_stopwatch(sw);
  T_ref += t_setup;
  sz = getsize_amatrix(G);
  Mem_ref += sz;
  printf("  %.2f seconds\n" "  %.1f MB\n" "  %.1f KB/DoF\n", 
         t_setup, sz / 1048576.0, sz / 1024.0 / points);
  alpha = dotprod_avector(r1,r2)/norm;
  printf("Computing norm\n");
  norm = norm2_avector(r2);
  alpha = alpha/norm;
  printf("  Euclidian norm %.3e\n", norm);
  printf("  norm - norm2 = %f; norm/norm2 = %f\n", norm - norm2, norm/norm2);
  printf("angle: %f + I* %f\n", creal(alpha), cimag(alpha));
  
  printf("Computing approximation error\n");
  add_avector(-1.0, r1, r2);
  error = norm2_avector(r2);
  printf("  Relative error %.3e (%.3e)\n", error/norm, error);
  printf(" %.2fs & %.1f MB & %.3e & %.2fs & %.1f MB\n", T_K, Mem_K / 1048576.0,error/norm, T_ref, Mem_ref / 1048576.0);
  
  printf ("Cleaning up\n");
  del_amatrix (G);
  del_h2matrix (Gh2);
  freeDirectionalIndices(sroot);
  del_spatialcluster (sroot);
  del_clusterbasis (ccb);
  del_clusterbasis (rcb);
  del_block (broot);
  del_spatialgeometry (sg);
  unInitPotentialModule(pm);
  printf ("%u spatial clusters still active\n", getactives_spatialcluster()- 2); //-2 since we set up partial spatial cluster that we deleted manually afterwards in the test functions for row and col transfer
  printf ("%u blocks still active\n", getactives_block());
  
  uninit_kips();

  return EXIT_SUCCESS;
}
