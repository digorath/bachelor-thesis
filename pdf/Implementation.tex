
\section{Implementierung}
Die Implementierung ist in zwei verschiedene Dateien geteilt.
Ein Teil enthält die Implementierung der sphärischen Harmonischen,
während der andere Teil sich mit der Umsetzung des Algorithmus mittels der KIPS-Bibliothek befasst.\\
Der Programmcode ist in einem \verb|git|-Repository unter \\
\verb|https://gitlab.com/digorath/bachelor-thesis| zu finden.\\
Die aktuellste Version befindet sich im \verb|master|-Branch.
\subsection{Sphärische Harmonische}
Die sphärischen Harmonischen sind mithilfe der assoziierten Legendre-Funk- tionen definiert, welche wiederum mithilfe der Legendre-Polynome definiert sind.
Für die Implementierung wird diese Struktur von aufeinander aufbauenden Funktionen beibehalten.
Das bedeutet, dass zunächst die Legendre-Polynome implementiert wurden.\\
\subsubsection{Legendre Polynome}
Hierfür wurde eine bekannte Rekursionsformel verwendet, um die Koeffizienten des Polynoms zu ermitteln.
Bezeichnet $\p_l$ das Legendre-Polynom vom Grad $l \in\N_0$, so hat die Rekursionsformel folgende Gestalt:
\begin{theorem}[Rekursionsformel für Legendre-Polynome, \cite{nedelec} Lemma 2.4.3]
  Für alle $x\in[-1,1]$ gilt
  \begin{equation}
    (l+1)\p_{l+1}(x) -(2l+1)x\p_l(x) + l\p_{l-1}(x) = 0
  \end{equation}
\end{theorem}

Da wir im Rahmen des Algorithmus den maximalen Grad kennen, welchen wir von diesen Polynomen benötigen,
berechnen wir die Koeffizienten der Legendre-Polynome über die Rekursionsformel,
indem wir die Koeffizienten für das Polynom vom Grad $0$ und vom Grad $1$ vorgeben,
und dann inkrementell die übrigen benötigten Koeffizienten ausrechnen. \\
Es wird dafür eine Schleife durchlaufen, in welcher die höchsten und niedrigsten Koeffizienten separat berechnet werden,
und für die Koeffizienten an denen beide Vorgänger beteiligt sind, wird eine weitere innere Schleife durchlaufen.\\
Wir speichern lediglich die Koeffizienten der Legendre-Polynome. Die Auswertung eines Polynoms in einem Punkt geschieht dann mithilfe einer Implementierung des Horner-Schemas. 
Die im Polynom vorkommenden Monome werden hierbei absteigend sortiert gespeichert, sodass der Koeffizient mit Index $0$ gerade zum höchsten vorkommenden Monom gehört.
\newpage
\lstset{breaklines = true, language = C, tabsize = 1}
\begin{lstlisting}
	for(k=1; k<maxDegree;k++) {
		coeffs[k+1][0] = (2.0 * k + 1.0)/(1.0 * k + 1.0) * coeffs[k][0];
		coeffs[k+1][1] = 0.0;
		for(l = 2; l <= k; l++) {
			coeffs[k+1][l] = (2.0 * k + 1.0)/(1.0 * k + 1.0) * coeffs[k][l] - (1.0 * k / (1.0 * k + 1.0))* coeffs[k-1][l-2];
		}
		coeffs[k+1][k+1] = -(1.0 * k / (1.0 * k + 1.0)) * coeffs[k-1][k-1];
	}
\end{lstlisting}

An dieser Stelle ist erwähnenswert, dass bei diesen Polynomen jeweils jeder zweite Koeffizient $0$ ist.
Daher könnte man an dieser Stelle Speicherplatz und Berechnungsschritte einsparen.\\
Da der maximal benötigte Grad jedoch klein ist, sogar im zweistelligen Bereich,
wurde an dieser Stelle jedoch die Leserlichkeit des Codes höher priorisiert als die daraus resultierende Ineffizienz.

\subsubsection{Assoziierte Legendre-Funktionen}
Die assoziierten Legendre-Funktionen sind, wie wir in Definition \ref{assoc} bereits gesehen haben, über Ableitungen der Legendre-Polynome definiert.

Da wir die Legendre-Polynome in Form ihrer Koeffizienten vorliegen haben, können wir leicht die Koeffizienten der Ableitungen bestimmen,
indem wir die Koeffizienten verschieben, und mit den nötigen Exponenten multiplizieren.\\
Dafür wurde die Funktion \verb|derive| geschrieben. Da wir die verschiedenen Ableitungen alle benötigen, wird dafür jeweils ein neues Koeffiziententupel mit der korrekten Länge angelegt.
\begin{lstlisting}
	real* derive(real* coeffs, int degree, int order) {
		int newDegree = degree - order;
		real* newCoeffs;
	
		if (newDegree < 0) {
			newCoeffs = (real*) allocmem(sizeof(real));
			newCoeffs[0] = 0.0;
			return newCoeffs;
		} else {
			newCoeffs = (real*) allocmem(sizeof(real) * (newDegree + 1));
			for(int k = 0; k <= newDegree; k++) {
				newCoeffs[k] = prodFromTo(degree - k - order + 1, degree - k) * coeffs[k];
			}
		}
		return newCoeffs;
	}
\end{lstlisting}
Um die assoziierten Legendre-Funktionen zu erhalten muss man an die Ableitungen jeweils nur noch passende Faktoren multiplizieren.
Dies wurde für sowohl positive Werte als auch für negative Werte von $m$ implementiert. Durch die Definition der sphärischen Harmonischen wird jedoch deutlich,
dass es geschickter ist, nur diejenigen mit positiven Werten für $m$ zu nutzen.
\subsubsection{Sphärische Harmonische}
Die sphärischen Harmonischen sind in \cite{green} wiefolgt definiert.
\begin{definition}[Sphärische Harmonische]
	\label{ylm}
	Sei $l\in\N_0$, $m\in\Z$ mit $|m|\leq l$ und $(\theta,\phi)\in(0,\pi)\times (0,2\pi)$. Dann heißt
	\begin{equation}
		Y_l^m(\theta,\phi) := (-1)^m \sqrt{\frac{(l-m)!}{(l+m)!}} e^{im\phi}\p_l^m(\cos\theta).
	\end{equation}
	\textit{sphärische Harmonische vom Grad} $l$.
\end{definition}
Hierbei ist zu beachten, dass sphärische Harmonische nicht überall einheitlich definiert werden.
In \cite{kwast} und \cite{nedelec} werden zum Beispiel Faktoren hinzugefügt, durch welche diese Funktionen bezüglich einer gewissen Norm normiert sind.\\
Die Kehrwerte dieser Faktoren, welche zur Normalisierung hinzugefügt werden, treten dann wieder in den Formeln zur Translation und Umwandlung der Multipolentwicklung und der lokalen Entwicklung auf,
sodass unnötige Rechenschritte erzeugt werden, die nicht nur Rechenzeit kosten, sondern auch Rundungsfehler einfügen, da mit Gleitkommawerten gerechnet werden muss. \\
Aus diesem Grund wurden diese Faktoren auch in dieser Arbeit in der Definition weggelassen. \\
Die Wurzelfaktoren in der Definition der sphärischen Harmonischen und der assoziierten Legendre-Funktionen mit negativen Werten von $m$ kürzen sich partiell wiefolgt:
\begin{equation}
	\sqrt{\frac{(l-m)!}{(l+m)!}} \cdot \frac{(l+m)!}{(l-m)!} = \sqrt{\frac{(l+m)!}{(l-m)!}} = \sqrt{\frac{(l-|m|)!}{(l+|m|)!}}.
\end{equation}
Daher wurde in der Implementierung jeweils mit dem Absolutbetrag für $m$ gerechnet, und es werden keine Koeffizienten für negative Werte von $m$ bestimmt.
\begin{lstlisting}
void setupCoefficients(int maxDegree) {
	real** tmp = calculateLegendreCoeff(maxDegree);
	associatedLegendreCoefficients = allocmem(sizeof(real**) * (maxDegree + 1));
	for(int l = 0; l <= maxDegree; l++) {
		associatedLegendreCoefficients[l] = allocmem(sizeof(real*) * (l + 1));
		for(int m = 0; m <= l; m++) {
			associatedLegendreCoefficients[l][m] = derive(tmp[l], l, abs(m));
		}
	}
	freemem(tmp);
}
\end{lstlisting}
Die Funktion \verb|setupCoefficients| muss am Anfang vor der ersten Auswertung von sphärischen Harmonischen einmal aufgerufen werden.\\
Die Auswertung der sphärischen Harmonischen besteht dann nur noch aus dem Zusammenfügen der jeweiligen Faktoren.

\subsection{Implementierung des Algorithmus mithilfe der KIPS-Bibliothek}
% \subsubsection{Hilfsfunktionen und Strukturen}
Wie bereits im Abschnitt über die KIPS-Bibliothek erwähnt wurde, korrespondieren die verschiedenen Blatt- und Transfermatrizen
zu der Multipolentwicklung des Potenzials und deren Translationen, sowie zu den Translationen der lokalen Entwicklung und deren Auswertung in den Partikelpositionen. \\
In den Formeln für die jeweiligen Entwicklungen und ihren Translationen werden Doppelsummen verwendet, um die Entwicklungen zu beschreiben. \\
Dadurch haben wir für die Koeffizienten der Entwicklungen Doppelindizes. Da wir jedoch die Koeffizienten des jeweils nächsten Schrittes mittels Matrixmultiplikation bestimmen möchten,
müssen wir diese Doppelindizes mit nur einem einzigen Index codieren. \\
Für die Kopplungsmatrizen wird die selbe Codierung verwendet. \\
Es ist jedoch sehr viel übersichtlicher mit den Doppelindizes arbeiten zu können, sodass dafür die Funktion \verb|combinedIndex| in \verb|potential.c| implementiert wurde.\\
Diese Funktion bestimmt für einen Doppelindex den dazugehörigen einzelnen Index. \\
Die dafür verwendete Formel ist \verb|combinedIndex(j,k) = j*j + j + k|,\\
 welche sich wiefolgt ergibt:
\begin{equation}
	\begin{array}{l}
		\forall l\in\N_0 : |\{k \in\Z | (l,k) \text{ ist ein vorkommender Index}\}| = 2l+1, \\
		\forall l\in\N_0 : \sum\limits_{l' < l} 2l'+1 = l^2.
	\end{array}
\end{equation}
Der Summand \verb|j| ist zusätzlich vorhanden, da die Indexmenge für \verb|k| gerade von \verb|-j| bis \verb|j| geht,
und man dadurch immer einen nicht negativen Wert auf \verb|j * j| addiert.\\

Eine weitere Funktion die zu Zwecken der Übersichtlichkeit und Reduktion der Code-Redundanz implementiert wurde,
berechnet die Koeffizienten $A_n^m$, welche in den Formeln für die Translation und Konvertierung vorkommen.
Da diese Koeffizienten häufig benötigt werden, wurde dazu zusätzlich eine globale Variable angelegt, sowie eine Funktion \verb|setupA|, welche in dieser globalen Variablen alle benötigten Koeffizienten $A_n^m$ abspeichert. \\
Dadurch ist innerhalb des Programms keine Mehrfachauswertungen der Koeffizienten mehr nötig.\\

Um die Bibliothek nutzen zu können, müssen verschiedene Funktionen implementiert werden.
\begin{itemize}
	\item Funktionen zum Befüllen der Blatt-, Transfer- und Kopplungsmatrizen.
	\item Funktionen zum Befüllen der Matrizen für unzulässige Blöcke.
	\item Funktionen zum Befüllen der Clusterbasen.
	\item Eine Funktion zum Befüllen der $\h^2$-Matrix.
\end{itemize}
Die Funktionen für die letzten beiden Punkte gehen lediglich den Clusterbaum, bzw. den Block-Clusterbaum rekursiv durch,
und befüllen die entsprechenden Einträge mit den Funktionen, welche in den ersten beiden Punkten genannt wurden.
All diese Funktionen benötigen jedoch einige gemeinsame Parameter, damit eindeutig ist wie die entsprechenden Teile befüllt werden müssen. \\
Diese Parameter werden in einem \verb|struct| namens \verb|potentialmatrix| abgelegt:
\newpage
\begin{lstlisting}
	struct _potentialmatrix {
  /** @brief Spatial dimension. */
  uint dim;
  /** @brief Kernel function. */
  potential g;
  /** @brief Number of points. */
  uint points;
  /** @brief Coordinates of points. */
  real **x;
  /** @brief Order of approximation. */
  int nu;
};
\end{lstlisting}
In unserem Fall ist die Dimension immer $3$, jedoch gibt es z.B. in \cite{green} auch Formeln für den zwei-dimensionalen Fall. \\
Die Kernfunktion ist in unserem Fall die Funktion
\begin{equation}
	\R^3\times\R^3 \rightarrow \R_{\geq 0}, (x,y) \mapsto \begin{cases} 0 \text{, falls x = y} \\
	\frac{1}{\|x-y\|_2} \text{, sonst.} \end{cases}
\end{equation}
Diese Funktion ist in der Funktion \verb|potential_3d| implementiert, welche dann \verb|potentialmatrix->g| zugewiesen wird.\\
Die Punkte werden zufällig erzeugt. \\

\subsubsection{Blattmatrizen}
Bei dem Befüllen der Blattmatrizen für die Spalten-Clusterbasis müssen wir berücksichtigen, dass diese komplex-konjugiert abgelegt werden.\\
Dadurch korrespondieren die Zeilen der Matrix, welche wir befüllen mit den Punkten im zugehörigen Cluster,
während die Spalten zu den Koeffizienten der sphärischen Harmonischen mit demselben codierten Index in der Multipolentwicklung gehören. \\
Auch für die Blattmatrizen für die Zeilen-Clusterbasis korrespondieren die Zeilen zu den Punkten, sodass in einer Zeile die ausgewerteten sphärischen Harmonischen in der Partikelposition gespeichert werden müssen.\\
Die Funktionen, die die Blattmatrizen befüllen, bekommen als Argumente den jeweiligen Cluster, die \verb|potentialmatrix| und einen Pointer auf eine \verb|amatrix|, welche sie befüllen.\\
Sie heißen \verb|fillV_col_potentialmatrix| und \verb|fillV_row_potentialmatrix|  und sind in \verb|potential.c| implementiert.\\
Die Strukturen der beiden Funktionen sind sehr ähnlich, daher gehen wir hier nur näher auf die von \verb|fillV_col_potentialmatrix| ein.\\
Nachdem die nötigen Variablen deklariert und initialisiert sind, wird zunächst der Mittelpunkt des Clusters bestimmt, indem man die Einträge \verb|bmin| und \verb|bmax| verwendet.\\
Anschließend beginnt eine Schleife, welche den Index \verb|i| für die Zeilen der Matrix durchläuft.
Innerhalb dieser Schleife liegt dann die Berechnung der sphärischen Koordinaten des zum Zeilenindex gehörenden Partikels in Bezug zum Clustermittelpunkt, sowie eine weitere Doppelschleife über die Doppelindizes der Koeffizienten der Multipolentwicklung. \\
Die sphärischen Koordinaten werden mittels einer Hilfsfunktion aus \verb|sphe-| \verb|rical_harmonics.c| berechnet. \\
Die Berechnung eines Koeffizienten sieht wiefolgt aus:
\begin{lstlisting}
	l = combinedIndex(n,m);
	Va[i + ld * l] = conj(powCN(sphericalCoords[0], n) * evalSpherical(n, -m, sphericalCoords[1], sphericalCoords[2]));
\end{lstlisting}
Hierbei ist die Funktion \verb|powCN| eine Hilfsfunktion, welche die Potenz einer komplexen Zahl mit natürlichem Exponenten berechnet.
\subsubsection{Transfermatrizen}
Die Funktionen zum Befüllen der Transfermatrizen sind insofern ähnlich zu den Blattmatrizen, die \verb|potentialmatrix| und eine Matrix übergeben bekommen, welche sie befüllen.
Sie bekommen jedoch nicht nur ein Cluster als Argument, sondern zwei, da sie den Transfer von einem Cluster in den anderen darstellen. \\
Für die Spaltenclusterbasis stellen sie den Transfer von dem Sohncluster in den Vatercluster dar, während sie in der Spaltenclusterbasis den Transfer vom Vatercluster in den Sohn repräsentieren.\\
Im Gegensatz zu den Blattmatrizen haben wir hier jedoch eine weitere wichtige Eigenschaft, die in der Implementierung ausgenutzt wurde: \\
Die Transfermatrizen sind nur von der relativen Position des Sohnclusters gegenüber des Vaterclusters abhängig! \\
Das bedeutet, dass viele Transfermatrizen identisch sind!\\
Es muss im Rahmen der genutzten Bibliothek zwar leider immer eine Kopie der Matrix angelegt werden, wir können jedoch den Rechenaufwand stark reduzieren,
indem wir nur kopieren, und nicht jedesmal dieselben Einträge neu berechnen. \\
Um dies effizient umzusetzen, muss man einen genaueren Blick auf die Implementierung der Verfeinerung der geometrischen Cluster werfen. \\
In KIPS ist es so implementiert, dass im Gegensatz zu der Formulierung der FMM nicht in allen Richtungen simultan verfeinert wird,
sondern stattdessen in jedem Verfeinerungsschritt nur in Richtung einer Achse alle Cluster in der Mitte teilt.\\
Da wir in drei Richtungen teilen können, und jeweils zwei Söhne entstehen, gibt es insgesamt 6 verschiedene Konfigurationen, wie ein Sohncluster bezüglich des Vaterclusters angeordnet sein kann.\\
Wir codieren die Richtungen daher mit einer Zahl von $0$ bis $5$. Dies geschieht in der Funktion \verb|sonDirection|, welche zwei Cluster als Argumente bekommt, und einen \verb|short| zurückliefert. \\
\begin{lstlisting}
short sonDirection(pcspatialcluster fc, pcspatialcluster sc) {
  if(fabs(fc->bmin[0]- sc->bmin[0]) > 0.0) return 0;
  if(fabs(fc->bmin[1]- sc->bmin[1]) > 0.0) return 1;
  if(fabs(fc->bmin[2]- sc->bmin[2]) > 0.0) return 2;
  if(fabs(fc->bmax[0]- sc->bmax[0]) > 0.0) return 3;
  if(fabs(fc->bmax[1]- sc->bmax[1]) > 0.0) return 4;
  if(fabs(fc->bmax[2]- sc->bmax[2]) > 0.0) return 5;
  return 6;
}
\end{lstlisting}
Die Codierung der Richtungen ist so angeordnet, dass die Richtung, die zu dem Index $(k+3) \mod 6$ gehört, gerade die entgegengesetzte Richtung ist, welche zu $k$ gehört.\\
Dies wurde in der Implementierung für die Transfermatrizen für die Reihen verwendet. \\
Diese Codierung für die Richtungen wurde nun so ausgenutzt, dass am Anfang einmal für jede Richtung eine Skelettmatrix aufgestellt wird, deren Einträge dann anschließend nur noch mit der entsprechenden Potenz des Abstandes der Mittelpunkte multipliziert werden müssen, um die Transferrmatrix zu erhalten.\\
Da hierbei ebenfalls sphärische Harmonische mehrfach in derselben Richtung ausgewertet werden, wird dafür ebenfalls eine globale Variable angelegt und initial einmal befüllt, sodass diese an den jeweiligen Stellen nur noch ausgelesen werden muss. \\
Desweiteren muss, wie bei den Blattmatrizen, berücksichtigt werden, dass die Matrix für die Spaltenclusterbasis komplex-konjugiert abgelegt werden muss.\\
Das Befüllen der Einträge eines Skelettes einer Transfermatrix sieht exemplarisch wiefolgt aus:\\
Es gibt 4 ineinanderliegende \verb|for|-Schleifen, wovon die äußeren beiden zu dem Doppelindex $(j,k)$ des neuen Koeffizienten in der Formel \eqref{MM:coeffs} für die Translation korrespondieren.
Die inneren beiden Schleifen korrespondieren zu dem Doppelindex $(n,m)$ der alten Koeffizienten in derselben Formel.
\begin{lstlisting}
	rowIndex = combinedIndex(j,k);
	colIndex = combinedIndex(j-n,k-m);
	entries[colIndex + rowIndex * rank] =
			conj(powCN(I, abs(k)-abs(m)-abs(k-m)) 
				* a[combinedIndex(n,m)] 
				* a[combinedIndex(j-n,k-m)]/a[combinedIndex(j,k)] 
				* smartHarmonics[l][combinedIndex(n, -m)]);
\end{lstlisting}
Die Variable \verb|rank| entspricht der Zeilen- bzw. Spaltenzahl der Matrix, und der Index \verb|l| der Variablen \verb|smartHarmonics| entspricht der codierten Richtung.
In der konkreten Implementierung wird der \verb|rowIndex| außerhalb der inneren beiden Schleifen berechnet. \\

\subsubsection{Kopplungsmatrizen}

Da bei der Befüllung der Kopplungsmatrizen dasselbe Problem auftritt, dass die Matrizen identisch sind, wenn die relativen Positionen eines Clusters zum anderen Cluster identisch sind,
wurde auch hier eine optimierte Variante implementiert, welche Mehrfachauswertungen stark reduziert. \\
Da wir jedoch in KIPS keine Interaktionsliste vorliegen haben, welche wir verwalten, sondern über eine Zulässigkeitsbedingung reguliert wird, für welche Paare von Clustern eine Kopplungsmatrix erzeugt wird,
kann man leider nicht für die möglichen Interaktionslisten Matrizen aufstellen. \\
Um eine Alternative zu ermöglichen wurde an dieser Stelle die KIPS-Biblio- thek leicht modifiziert: \\
Das \verb|struct spatialcluster|, welches die Cluster beschreibt, hat einen weiteren Eintrag \verb|directionalIndex *dir| erhalten.
Ein \verb|directionalIndex| ist hierbei eine Liste mit \verb|short|-Einträgen:
\begin{lstlisting}
struct _directionalIndex {
  directionalIndex *fatherDir;
  short idx;
};
\end{lstlisting}
Diese Einträge werden nach erstellen des Clusterbaumes belegt, indem man von der Wurzel des Clusterbaumes rekursiv den Baum durchläuft. In \verb|idx| wird der codierte Index für die relative Position zum Vater hinterlegt, und in \verb|fatherDir| wird ein Pointer auf den Eintrag des Vaters gesetzt.\\
Dies ermöglicht es uns die relativen Positionen von zwei Clustern zu identifizieren. \\
Da es von den Parametern der Zulässigkeitsbedingung abhängt, wie weit der letzte gemeinsame Vorfahr von einem zulässigen Paar von Clustern zurück- liegt,
und da es relevant ist, auf welcher Verfeinerungsebene sich die Cluster befinden, da nicht immer in alle Richtungen simultan verfeinert wird,
wurde an dieser Stelle lediglich eine Optimierung für verschiedene Clusterpaare auf der jeweils selben Verfeinerungsebene implementiert. \\
Um dies zu tun, wurde zunächst eine Funktion geschrieben, welche die relativen Positionen von Clusterpaaren in einem \verb|uint| codiert.
Diese geht iterativ von den betrachteten Clustern durch die Liste \verb|directionalIndex|, bis man bei der Wurzel des Clusterbaumes angekommen ist.\\
In jedem Schritt wird dann überprüft, wie die beiden Cluster zu ihrem jeweiligen Vater liegen.\\
Dabei gibt es drei Fälle die zu unterscheiden sind. Seien dafür die Sohncluster mit $A$ und $B$ bezeichnet.
\begin{itemize}
	\item Beide Sohncluster liegen gleich in Relation zum Vatercluster.
	\item Der Sohncluster $A$ ist der erste Sohn seines Vaters, während $B$ der zweite Sohn seines Vaters ist.
	\item Der Sohncluster $A$ ist der zweite Sohn seines Vaters, während $B$ der erste Sohn seines Vaters ist.
\end{itemize}
Die Unterscheidung in diese drei Fälle ist ausreichend, da in jedem Verfeinerungsschritt alle Cluster auf dieselbe Weise unterteilt werden.\\
In der Implementierung wurde dies umgesetzt, indem man die Ordnung der Einträge \verb|directionalIndex->idx| vergleicht. \\
Das Funktionsschema der Funktion, die den Gesamtindex berechnet, sieht in Pseudocode wiefolgt aus.
\begin{lstlisting}
	idx1 = A->dir;
	idx2 = B->dir;
	result = 0;
	while (fathersExist) {
		result = result*3;
		if(zweiterFall) result = result + 1;
		if(dritterFall) result = result + 2;
		idx1 = idx1->fatherDir;
		idx2 = idx2->fatherDir;
	}
\end{lstlisting}
Die Kopplunngsmatrix ist nur von diesem Index abhängig. Daher wird beim befüllen der Kopplungsmatrizen überprüft, ob bereits eine solche Matrix berechnet wurde.
Wenn dies der Fall ist, so wird diese kopiert. Falls nicht, so wird eine neue Matrix angelegt und anschließend kopiert.
Im Rahmen der Implementierung wurde dafür eine Liste konstruiert, in der jedes Listenelement zwei Einträge besitzt. Zum einen ein \verb|uint|, welcher dem Richtungsindex entspricht, und zum anderen einen Pointer auf eine \verb|amatrix|.\\
Es wird immer erst in der Liste nach einer bereits vorhandenen passenden Matrix gesucht, bevor im Falle des Misserfolgs eine neue Matrix berechnet wird, welche dann der Liste zusammen mit ihrem Index angehängt wird.

\subsubsection{Unzulässige Blöcke}
Für unzulässige Blöcke muss die vollbesetzte Matrix berechnet werden, welche zu den Partikeln in den jeweiligen Clustern gehören.\\
Dafür wird einfach durch alle Paare von Partikeln durchiteriert und das Potenzial berechnet, welches dann in einer Matrix abgespeichert wird.\\
Die Funktion, welche hierfür implementiert wurde, wird ebenfalls verwendet um die Referenzergebnisse im Abschnitt über die numerischen Resultate zu erzeugen.