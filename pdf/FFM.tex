\section{Das schnelle Multipolverfahren}
\subsection{Benötigte Begriffe}
Das Modellproblem, welches wir im Rahmen der Implementierung lösen möch- ten, ist wiefolgt formuliert:
\begin{itemize}
  \item Es gibt eine Menge von Partikeln, welche jeweils mit einer Ladung assoziiert sind.
  \item Die geladenen Partikel befinden sich in einem Würfel der Seitenlänge $1$, welcher um den Ursprung zentriert ist, und dessen Seiten achsenparallel sind.
  \item Das Ziel ist es, das Potenzial, welches von den geladenen Partikeln induziert wird, in den Positionen zu approximieren.
\end{itemize}
Wir formulieren den Algorithmus zunächst so, wie er in \cite{kwast} zu finden ist.
Diese Formulierung ist angelehnt an die Formulierung in \cite{green}. \\
Damit der Algorithmus formuliert werden kann, benötigen wir jedoch noch einige weitere Begriffe.

Der Würfel, in dem sich alle Partikel befinden, wird von dem Algorithmus in kleinere Würfel unterteilt.
Wir nennen den gesamten Würfel die Box auf (Verfeinerungs-)Ebene $0$. \\
Haben wir eine Box $b$ auf Ebene $l$, so erhalten wir daraus 8 Boxen auf Ebene $l+1$, indem wir $b$ entlang jeder Seite in der Mitte unterteilen.
Diese 8 Boxen heißen \textit{Kinder(-boxen)} von $b$, und $b$ heißt \textit{Elternbox} oder auch \textit{Vaterbox}.\\
Diese Verfeinerung wird solang fortgeführt, bis man auf feinster Ebene etwa so viele Boxen vorliegen hat, wie es Partikel gibt.
Das bedeutet, dass die feinste Ebene in etwa $n \approx log_8(N)$ ist, sofern $N$ die Anzahl der Partikel ist. \\
Durch diese Unterteilung und Benennung bekommt man eine Intuitive Baumstruktur, in welcher die Box auf Ebene $0$ gerade der Wurzel entspricht,
während die Boxen auf feinster Ebene die Blätter sind.\\
Im Rahmen des Algorithmus werden zwei Arten der Entwicklung der Potenzialfunktion in Reihen verwendet.
Zum einen ist es die namensgebende Multipolentwicklung,
und zum anderen ist es eine lokale Entwicklung, welche stark mit der Multipolentwicklung zusammenhängt.\\
Wie diese beiden Entwicklungen genau aussehen, wird später erläutert. \\
Zur erstmaligen Formulierung des Algorithmus ist jedoch wichtig, dass man einige Eigenschaften der Entwicklungen kennt, die wir hier nun abstrakt formulieren.
\begin{itemize}
  \item Sowohl die Multipolentwicklung als auch die lokale Entwicklung sind Entwicklungen einer Funktion um ein gewisses Zentrum.
  \item Dieses Entwicklungszentrum kann sowohl für lokale Entwicklungen als auch für Multipolentwicklungen verschoben werden.
  \item Man kann eine Multipolentwicklung um ein Zentrum in eine lokale Entwicklung um ein anderes Zentrum umwandeln.
\end{itemize}

Desweiteren benötigen wir noch den Nachbarschaftsbegriff für Boxen und den Begriff der Interaktionsliste. \\
\begin{itemize}
  \item \textbf{Nachbar:} Zwei Boxen heißen \textit{benachbart}, wenn sie auf derselben Verfeinerungsebene sind, und einen gemeinsamen Randpunkt besitzen.
  \item \textbf{Interaktionsliste:} Die Interaktionsliste einer Box $b$ besteht aus den Kindern der Nachbarn der Elternbox, welche nicht zu $b$ benachbart sind.
\end{itemize}
\begin{figure}
	\includegraphics[width=\linewidth]{Diagrams/InteractionList.jpg}
	\caption{Interaktionsliste einer Box}
\end{figure}
\subsection{Der Algorithmus}
\subsubsection{Abstrakte Formulierung}
Wir werden mithilfe der zuvor eingefühtren Bezeichnungen nun das schnelle Multipolverfahren formulieren.
Anschließend werden wir die Details der einzelnen Schritte diskutieren, und die konkreten benötigten Formeln angeben. \\
Vor dem ersten Schritt werden die Verfeinerungstiefe (üblicherweise in Abhän- gigkeit der Anzahl der Partikel) und die Ordnung der Entwicklungen (in Abhängigkeit der gewünschten Präzision) festgelegt.

\par
\textbf{Schritt 1}\\
Berechne die Koeffizienten der Multipolentwicklung $\phi_{n,j}$ für jede Box $j$ auf feinster Ebene $n$, welche das Potenzial approximiert, welches von den Partikeln in der jeweiligen Box induziert wird.
\par
\textbf{Schritt 2} \\
Für alle $l$ von $n-1$ bis $0$:
\par
\addtolength{\leftskip}{5mm}
Für jede Box $j$ auf Ebene $l$:
\par
\addtolength{\leftskip}{5mm}
Berechne die Koeffizienten der Multipolentwicklung $\phi_{l,j}$ durch \\
Verschieben des Entwicklungszentrums von $\phi_{l+1,k}$ zum Mittelpunkt von $j$ für jedes Kind $k$ von $j$ und aufaddieren der Ergebnisse.
\par
\addtolength{\leftskip}{-10mm}
\textbf{Schritt 3}\\
Initialisiere für jede Verfeinerungsebene $l$ und alle Boxen $j$ auf Ebene $l$ die Koeffizienten der lokalen Entwicklung $\psi_{l,j} := 0$.
\par
\textbf{Schritt 4}\\
Für jedes $l$ von $0$ bis $n$:
\par
\addtolength{\leftskip}{5mm}
Für jede Box $j$ auf Ebene $l$: 
\par
\addtolength{\leftskip}{5mm}
Konvertiere für jede Box $k$ in der Interaktionsliste von $j$ die Multipolentwicklung $\phi_{l,k}$ in eine lokale Entwicklung um das Zentrum von $j$ und addiere das Ergebnis zu $\psi_{l,j}$.
\par
\addtolength{\leftskip}{-10mm}
\textbf{Schritt 5}\\
Für jedes $l$ von $0$ bis $n-1$:
\par
\addtolength{\leftskip}{5mm}
Für jede Box $k$ auf Ebene $l$: 
\par
\addtolength{\leftskip}{5mm}
Berechne für jedes Kind $j$ von $k$ die Koeffizienten von $\psi_{l+1,j}$ durch Verschieben des Entwicklungszentrums von $\psi_{l,k}$ zum Mittelpunkt des Kindes und hinzuaddieren zu $\psi_{l+1,j}$.
\par
\addtolength{\leftskip}{-10mm}
\textbf{Schritt 6}\\
Für jede Box $j$ auf feinster Ebene:
\par
\addtolength{\leftskip}{5mm}
Werte für jedes Partikel in Box $j$ an dessen Position die lokale Entwicklung $\psi_{n,j}$ aus.
\par
\addtolength{\leftskip}{-5mm}
\textbf{Schritt 7}\\
Für jede Box $j$ auf feinster Ebene:
\par
\addtolength{\leftskip}{5mm}
Berechne für jedes Partikel in $j$ die Interaktion mit allen anderen Partikeln in Box $j$ und deren Nachbarn, also alle Interaktionen, welche nicht durch $\psi_{n,j}$ abgedeckt sind, und addiere das Ergebnis zu dem von Schritt 6.
\par
\addtolength{\leftskip}{-5mm}

%% konkretere diskussion der schritte des algorithmus mittels der formeln aus 3dsource + abschätzungen
\subsubsection{Sphärische Harmonische}
Damit wir die notwendigen Formeln für die einzelnen Schritte, wie sie in \cite{src3d} zu finden sind, diskutieren können,
müssen wir die \textit{sphärischen Harmonischen} einführen, da diese Funktionen in fast jedem der Schritte verwendet werden.\\
Diese speziellen Funktionen sind mithilfe der \textit{Legendre-Polynome} und den dazugehörigen \textit{assoziierten Legendre-Funktionen} definiert.\\
Es ist daher sinnvoll zunächst mit den \textit{Legendre-Polynomen} anzufangen.\\
Die Legendre-Polynome kann man auf viele verschiedene Weisen definieren, z.B. über anwenden des Gram-Schmidtschen Orthogonalisierungsverfahrens auf die Monombasis oder über Rekursionsformeln. \\
Wir werden hier jedoch eine geschlossene Form wählen, welche bekannt ist als die Rodrigues-Formel bekannt ist. \\
Diese Definition findet man z.B. auch in \cite{nedelec}.
\begin{definition}[Legendre-Polynom]
  Sei $l\in\N_0$. Dann heißt
  \begin{equation}
    \p_l: \R \rightarrow \R, \p_l(x) = \frac{(-1)^l}{2^l l!} \left( \frac{d}{dx} \right)^l (1-x^2)^l
  \end{equation}
  das \textit{Legendre-Polyonom vom Grad l}.
\end{definition}

Die \textit{assoziierten Legendre-Funktionen} sind mit diesen Polynomen eng verwandt.
\begin{definition}[Assoziierte Legendre-Funktion]
  \label{assoc}
  Seien $l,m\in\N_0$ mit $m \leq l$. Dann heißen
  \begin{equation}
    \begin{array}{lrl}
      \p_l^m: [-1,1] \rightarrow \R, &\p_l^m(x) = & \left(\sqrt{1-x^2}\right)^m\left( \frac{d}{dx} \right) \p_l(x) \\
      \p_l^{-m}: [-1,1] \rightarrow \R, &\p_l^{-m}(x) = & \frac{(l-m)!}{(l+m)!} \p_l^m(x)
    \end{array}
  \end{equation}
  \textit{assoziierte Legendre-Funktion der Ordnung l}.
\end{definition}

Wir können nun die \textit{sphärischen Harmonischen} definieren. Wir werden dazu die Definition aus \cite{green} verwenden, wie sie auch in \cite{src3d} verwendet wird.
\begin{definition}[Sphärische Harmonische]
  Seien $l,\in\N_0$ und $m\in\Z$ mit $|m| \leq l$. Dann heißt
  \begin{equation}
    Y_l^m: [0,\pi]\times[-\pi,\pi] \rightarrow \C, Y_l^m(\theta,\phi) = \sqrt{\frac{(l-|m|)!}{(l+|m|)!}}\p_n^{|m|}(\cos \theta) e^{im\phi}
  \end{equation}
  \textit{sphärische Harmonische der Ordnung l}.
\end{definition}

Wir haben nun alle Definitionen zur Verfügung, um die im Algorithmus verwendeten Formeln genauer hinzuschreiben, und zu diskutieren.

\subsubsection{Details der Schritte des Algorithmus}
Wir werden nun die Details der einzelnen Schritte durchgehen. Wir werden innerhalb der Diskussion der einzelnen Schritte bereits den Rechenaufwand diskutieren, welchen wir anschließend nur noch zusammenfassen werden.
Die Diskussion des erwarteten Fehlers wird am Ende dieses Abschnitts durchgeführt.
Für diesen Abschnitt gehen wir davon aus, dass die Anzahl der Partikel in unserem betrachteten Problem mit $N$ bezeichnet ist,
und die feinste Verfeinerungsebene $n \approx \log_8(N)$ ist.\\
Die Ordnung der Entwicklungen bezeichnen wir mit $\nu$. Wie diese eingeht, ist in der genaueren Diskussion der einzelnen Schritte ersichtlich.
\par \ \\
%schritt 1: Multipolentwicklung für ein cluster
\textbf{Schritt 1: Multipolentwicklung auf feinster Ebene}\\
Im ersten Schritt berechnen wir die Koeffizienten der Multipolentwicklungen für jede Box auf feinster Ebene.\\
Um die Details der Berechnung zu diskutieren, erläutern wir nun, die Multipolentwicklung.
Sei also $Q$ die Menge der Partikel(-positionen), welche in einer Kugel mit Radius $a$ um den Ursprung enthalten ist, 
sowie $c_q\in\R$ jeweils die Ladung des Partikels für jedes Partikel $q\in Q$. Sei $p\in\R^3$ mit $\|p\|_2 > a$.\\
Seien darüber hinaus $(\rho_q, \alpha_q, \beta_q)$ die sphärischen Koordinaten von $q$ für alle $q\in Q$
und $(r,\theta,\phi)$ die sphärischen Koordinaten von $p$.
Die Multipolentwicklung sieht dann wiefolgt aus:
\begin{equation}
  \Phi(p) = \sum\limits_{j=0}^{\infty}\sum\limits_{k=-j}^{j} M_j^k \frac{Y_j^k(\theta,\phi)}{r^{j+1}},
\end{equation}
wobei
\begin{equation}
  M_j^k = \sum\limits_{q\in Q} c_q \rho_q^j Y_j^{-k}(\alpha_q,\beta_q).
\end{equation}
Die vor dem ersten Schritt festgelegte Ordnung $\nu$ der Entwicklungen legt hier fest, ab welchem Summationsindex die Reihe abgeschnitten wird.\\
Der dadurch entstehende Fehler wird später diskutiert.\\
% In \cite{src3d} wird für das Abschneiden der Reihe folgende Fehlerabschätzung formuliert:
% \begin{equation}
%   \left|\Phi(p)- \sum\limits_{j=0}^{\nu}\sum\limits_{k=-j}^{j} M_j^k \frac{Y_j^k(\theta,\phi)}{r^{j+1}}\right| \leq \frac{A}{r-a}\left(\frac{a}{r}\right)^{\nu+1}
% \end{equation}
% Hierbei ist $A$ definiert als die Summe der Beträge der Ladungen der Partikel.
% Diese Abschätzung wird an späterer Stelle in den Algorithmus einfließen. \\
Wir werden nun den Rechenaufwand des ersten Schrittes abschätzen.\\
Da wir zu diesem Zeitpunkt nicht an der Auswertung der Multipolentwicklung in einem bestimmten Punkt interessiert sind,
müssen wir den Term $\frac{Y_j^k(\theta,\phi)}{r^{j+1}}$ nicht berechnen. \\
Die einzigen Werte, die in diesem Schritt berechnet werden, sind die Terme $M_j^k$ für $j\in\N_0$ mit $j\leq \nu$ und $k\in\Z$ mit $|k|\leq j$.\\
Die Anzahl der Kombinationen der Indizes ist die Summe der ungeraden Zahlen bis einschließlich $2\nu + 1$.
Diese Summe lässt sich berechnen als $(\nu+1)^2$. \\
In der Berechnung des Koeffizienten gibt es einen Summanden für jedes Partikel in der zugehörigen Box.
Da wir dies für jede Box auf der feinsten Ebene berechnen, gibt es insgesamt bei der Berechnung aller Koeffizienten genau einen Summanden für jedes der $N$ Partikel und jede Kombination der Indizes.\\
Insgesamt erhalten wir also $N \cdot (\nu + 1)^2$ Summanden, die berechnet werden müssen.
Für die Berechnung eines einzelnen Summanden muss eine sphärische Harmonische, eine Potenz mit natürlichem Exponenten, sowie das Produkt dreier Zahlen berechnet werden. \\
Desweiteren müssen die sphärischen Koordinaten unserer Partikelpositionen in Relation zu dem Boxmittelpunkt als Urprung bestimmt werden.
Dies muss jedoch nur einmal pro Partikel geschehen, da es unabhängig von der Belegung der Indizes ist.\\
Den Rechenaufwand für die Auswertung einer Potenz kann man grob durch eine Konstante multipliziert mit dem Exponenten beschränken und liegt somit auf jeden Fall in $\O(\nu)$. \\
Auch wenn die Auswertung einer sphärischen Harmonischen dahingegen deutlich aufwendiger ist, liegt der Rechenaufwand der Implementierung, die in einem späteren Abschnitt erörtert wird, immernoch in $\O(\nu)$.\\
%wie man leicht sehen kann, da nur eine Schleife verwendet wird, welche pro Schleifendurchlauf konstanten Aufwand hat und die Anzahl der Schleifendurchläufe durch $\nu$ beschränkt ist.\\
Wir werden unsere Abschätzungen zum Rechenaufwand daher zunächst mit dieser Abschätzung für den Aufwand der Berechnung einer sphärischen Harmonischen formulieren. \\
Es ist also der gesamte Rechenaufwand von Schritt 1 in $\O(N\cdot\nu^3)$. \\
Nachdem wir alle Schritte im Detail erläutert haben, werden wir noch darauf hinweisen, wie man den durch die im Laufe der Schritte anfallenden Auswertungen der sphärischen Harmonischen entstehenden Rechenaufwand unabhängig von deren Implementierung reduzieren kann.\\
% Da für jede Box auf feinster Ebene die Koeffizienten $M_j^k$ gespeichert werden, ist der Speicheraufwand in $\O(8^{\log_8(N)} \cdot \nu^2) = \O(N \cdot\nu^2)$.

\par \ \\
\textbf{Schritt 2: Translation der Multipolentwicklungen}\\
In diesem Schritt berechnen wir die Multipolentwicklungen für alle anderen Boxen, indem wir angefangen bei der feinsten Ebene die Entwicklungen in den Vater verschieben. \\
Das bedeutet, dass wir für jede Box, die einen Vater hat, eine Translation einer Multipolentwicklung durchführen müssen.
Die dafür benötigten Formeln sind ebenfalls in \cite{src3d} zu finden. \\
Seien dazu $q$ der Mittelpunkt einer Sphäre $D$ mit Radius $a$, und $(\rho,\alpha,\beta)$ die sphärischen Koordinaten von $q$. \\
Sei für alle $p$ außerhalb von $D$ die Multipolentwicklung für geladene Partikel innerhalb von $D$ gegeben durch
\begin{equation}
  \Phi(p) = \sum\limits_{j=0}^{\infty}\sum\limits_{k=-j}^{j} L_j^k \frac{Y_j^k(\theta',\phi')}{r'^{j+1}},
\end{equation}
wobei $(r',\theta',\phi')$ die sphärischen Koordinaten von $p$ bezüglich $q$ als Ursprung seien.\\
Dann ist für alle $p$ außerhalb der Sphäre mit Radius $a+\rho$ um den Ursprung
\begin{equation}
  \Phi(p) = \sum\limits_{j=0}^{\infty}\sum\limits_{k=-j}^{j} M_j^k \frac{Y_j^k(\theta,\phi)}{r^{j+1}},
\end{equation}
wobei
\begin{equation}
  \label{MM:coeffs}
  M_j^k := \sum\limits_{l=0}^j\sum\limits_{m=-l}^l \frac{L_{j-l}^{k-m} i^{|k|-|m|-|k-m|} A_l^m A_{j-l}^{k-m}}{A_j^k} \cdot \rho^l Y_l^{-m}(\alpha,\beta).
\end{equation}
Die hier vorkommenden Koeffizienten $A_l^m$ werden auch in den weiteren Schritten wieder vorkommen, und sind wiefolgt definiert:
\begin{equation}
\label{alm}
A_l^m := \frac{(-1)^l}{\sqrt{(l-m)!(l+m)!}}.
\end{equation}
In der Summe in Gleichung \eqref{MM:coeffs} werden Belegungen für Indizes verwendet, für welche $L_{j-l}^{k-m}$ nicht definiert ist. In diesen Fällen ist der Summand $0$.\\
Wollen wir nun die abgeschnittene Reihe mithilfe dieser Formel berechnen, fällt auf, dass wir tatsächlich auch nur die Koeffizienten der ursprünglichen Reihe bis zum selben Grad benötigen.
Wenn wir diese Formel also benutzen um unsere abgeschnittene Multipolentwicklungen von den Kindern in die Väter zu verschieben, erhalten wir dieselbe Entwicklung, als wenn wir diese direkt in dem Vater berechnet hätten. \\
Diese Erkenntnis geht später in die Fehlerabschätzung ein.\\
Da die Koeffizienten $A_l^m$ unabhängig von den Ladungs- und Partikelverteilungen sind, kann man diese am Anfang einmal berechnen und abspeichern, sodass kein zusätzlicher Rechenaufwand pro Translation erzeugt wird. \\
Der Term $\rho^l Y_l^{-m}(\alpha,\beta)$ ist unabhängig von den Indizes $j$ und $k$, sodass dieser für jede Kombination von $l$ und $m$ einmal berechnet werden kann.\\
Der Rechenaufwand pro Translation ist also abzuschätzen durch die Anzahl der Koeffizienten multipliziert mit der Anzahl der Summanden pro Koeffizient, und liegt somit in $\O(\nu^4)$.
Die Anzahl der Translationen ist eine pro Box, welche einen Vater hat, was gerade
\begin{equation}
  \sum\limits_{l=1}^{n} 8^l < 8^{(n+1)} \approx 8\cdot N
\end{equation}
entspricht, was in $\O(N)$ liegt. \\
Zusammen ergibt sich also ein Rechenaufwand, welcher in $\O(N\cdot \nu^4)$ liegt.

\newpage
\textbf{Schritt 3: Initialisierung der lokalen Entwicklungen}\\
In diesem Schritt wird keine arithmetische Berechnung vorgenommen, jedoch wird Speicher angefordert und bereinigt,
was einen Aufwand von $\O(N\cdot \nu^2)$ hat, da es für jede Box gemacht wird, und jeweils $\O(\nu^2)$ Koeffizienten initialisiert werden.

\par \ \\
\textbf{Schritt 4: Konvertierung der Multipolentwicklungen in lokale Entwicklungen}\\
In diesem Schritt konvertieren wir die zuvor berechneten Koeffizienten für die Multipolentwicklungen in Koeffizienten lokaler Entwicklungen.\\
Sei dazu wie in der Diskussion zu Schritt 2 $D$ eine Sphäre mit Radius $a$ um einen Punkt $Q$ dessen sphärische Koordinaten durch $(\rho,\alpha,\beta)$ gegeben sind.\\
Diesmal gelte zusätzlich die Forderung 
\begin{equation}
  \label{distanceCond}
  \rho > (c+1)a
\end{equation}
für irgendeine Konstante $c\in \R_{>0}$.
Sei wie zuvor für Punkte $p$ außerhalb der Sphäre $D$ die Multipolentwicklung für Ladungen innerhalb der Sphäre gegeben durch
\begin{equation}
  \Phi(p) = \sum\limits_{j=0}^{\infty}\sum\limits_{k=-j}^{j} L_j^k \frac{Y_j^k(\theta',\phi')}{r'^{j+1}},
\end{equation}
wobei $(r',\theta',\phi')$ wieder die sphärischen Koordinaten von $p$ bezüglich $q$ als Ursprung bezeichnen.\\
Die zusätzliche Forderung \eqref{distanceCond} stellt nun sicher, dass die Multipolentwicklung in der Sphäre $D_0$ mit Radius $a$ um den Ursprung konvergiert. \\
Mithilfe einer weiteren Formel aus \cite{src3d} Punkte $p\in D_0$ können wir die Multipolentwicklung nun als lokale Entwicklung in der folgenden Form schreiben:
\begin{equation}
  \Phi(p) = \sum\limits_{j=0}^{\infty}\sum\limits_{k=-j}^{j} M_j^k Y_j^k(\theta,\phi)r^{j},
\end{equation}
wobei in diesem Fall die neuen Koeffizienten durch
\begin{equation}
  \label{ML:coeffs}
  M_j^k := \sum\limits_{l=0}^\infty\sum\limits_{m=-l}^l \frac{L_{l}^{m} i^{|k-m|-|k|-|m|} A_l^m A_{j}^{k}}{(-1)^l A_{j+l}^{m-k}} \cdot \frac{Y_{j+l}^{m-k}(\alpha,\beta)}{\rho^{j+l+1}}
\end{equation}
defniniert sind.\\
Im Algorithmus schneiden wir diese Reihenentwicklung für die lokale Entwicklung erneut bei $l=\nu$ ab, wodurch ein neuer Fehler eingeführt wird,
welchen wir am Ende dieses Abschnittes diskutieren. \\
Diese Formel wenden wir nun für jede Box $b$ auf jede Multipolentwicklung an, die zu einer Box in der Interaktionsliste von $b$ gehört. \\
Die Größe der Interaktionsliste für eine Box ist am größten, wenn der Vater maximal viele Nachbarn besitzt.\\
Für eine Box $b$ ist die maximale Anzahl an Nachbarn des Vaters 26. Jeder dieser Nachbarn hat wiederum 8 Kinder. Von diesen 208 Kindern sind jedoch 19 benachbart zu $b$ selbst, sodass die Interaktionsliste maximal 189 einträge hat.
Befindet sich der Vater von $b$ am Rand der Box auf gröbster Verfeinerungsebene, so sind es weniger. \\
Da wir abgeschnittene Multipolentwicklungen konvertieren, ist die Anzahl der Summanden der Formeln für die Koeffizienten der lokalen Entwicklung wieder in $\O(\nu^2)$.
Wir müssen hier für jeden Summanden allerdings eine sphärische Harmonische auswerten, sodass wir einen zusätzlichen Aufwand bekommen, welcher in $\O(\nu)$ liegt.
Die Anzahl der Koeffizienten ist ebenfalls in $\O(\nu^2)$.
Wir führen diese Konvertierung für jede Box mit nicht leerer Interaktionsliste durch, sodass der Rechenaufwand in $\O(N\cdot \nu^5)$ liegt.
%todo TODO check rekursionsformel in 88a, ob man sich das sparen kann, indem man in geschickter reihenfolge berechnet
\par \ \\
\textbf{Schritt 5: Translation der lokalen Entwicklungen}\\
In diesem Schritt verschieben wir die in Schritt 4 berechneten lokalen Entwicklungen für jede Box in ihre Söhne, sofern diese existieren,
und addieren die verschobene Entwicklung zu der in Schritt 4 für die Söhne berechneten lokalen Entwicklungen hinzu.\\
Sei dafür diesmal der Punkt $q$ mit sphärischen $(\rho,\alpha,\beta)$ das Zentrum einer lokalen Entwicklung, welche für Punkte $p$ mit sphärischen Koordinaten \\
$(r',\theta',\phi')$ bezüglich $q$ als Ursprung durch
\begin{equation}
  \Phi(p) = \sum\limits_{j=0}^{\nu}\sum\limits_{k=-j}^{j} L_j^k Y_j^k(\theta',\phi')r'^{j}.
\end{equation}
gegeben ist.\\
Dann ist lässt sich diese lokale Entwicklung mittels einer weiteren Formel aus \cite{src3d} als lokale Entwicklung um den Ursprung schreiben, und es ergibt sich für Punkte $p$ mit sphärischen Koordinaten $(r,\theta,\phi)$
\begin{equation}
  \Phi(p) = \sum\limits_{j=0}^{\nu}\sum\limits_{k=-j}^{j} M_j^k Y_j^k(\theta,\phi)r^{j},
\end{equation}
wobei die neuen Koeffizienten $M_j^k$ definiert sind durch:
\begin{equation}
  \label{LL:coeffs}
  M_j^k := \sum\limits_{l=0}^\nu\sum\limits_{m=-l}^l \frac{L_{l}^{m} i^{|m|-|m-k|-|k|} A_{l-j}^{m-k} A_{j}^{k}}{(-1)^{l+j} A_{l}^{m}} \cdot Y_{l-j}^{m-k}(\alpha,\beta)\rho^{l-j}.
\end{equation}
Analog zu Schritt 2 gibt es hier Kombinationen von Indizes, sodass $Y_{l-j}^{m-k}$ nicht definiert ist. Auch hier sind die betroffenen Summanden jeweils 0.\\
Für eine Translation müssen auch hier $\O(\nu^2)$ neue Koeffizienten berechnet werden, in deren Berechnungen jeweils $\O(\nu^2)$ Summanden berechnet werden müssen.
In jedem der Summanden muss auch hier eine sphärische Harmonische ausgewertet werden, sodass ein weiterer Rechenaufwand in $\O(\nu)$ hinzukommt.
Da wir wieder für jede Box, die einen Vater hat, eine Translation vornehmen, ergibt sich also ein Rechenaufwand in $\O(N\cdot\nu^5)$.

\par \ \\
\textbf{Schritt 6: Auswertung der lokalen Entwicklungen}\\
In diesem Schritt werten wir für jede Box auf feinster Verfeinerungsebene die dazugehörige lokale Entwicklung in jedem in der Box enthaltenen Partikel aus.
Da eine solche lokale Entwicklung die Gestalt
\begin{equation}
  \Phi(p) = \sum\limits_{j=0}^{\nu}\sum\limits_{k=-j}^{j} M_j^k Y_j^k(\theta,\phi)r^{j}
\end{equation}
hat, müssen wir für jeden Punkt, in welchem wir diese auswerten wollen $\O(\nu^2)$ Summanden berechnen,
wobei in jedem der Summanden eine sphä- rische Harmonische vorkommt, welche ausgewertet werden muss.\\
Da wir in der Position jedes Partikels in der Box auswerten möchten, müssen wir jeweils die sphärischen Koordinaten der Partikelpositionen bezüglich des Boxmittelpunkts ermitteln
und anschließend sämtliche sphärischen Harmonischen dort auswerten, und mit der entsprechenden Potenz des Radius multiplizieren.\\
Es ergibt sich daher ein Rechenaufwand von $\O(N\cdot \nu^3)$, wenn wir dies für alle Boxen auf feinster Ebene auswerten.

\par \ \\
\textbf{Schritt 7: Auswertung der Nahfeldterme}\\
In diesem letzten Schritt berechnen wir die Interaktionen zwischen Partikeln innerhalb einer Box auf feinster Verfeinerungsebene, sowie innerhalb der Nachbarn,
da diese Interaktionen genau diejenigen sind, welche in den vorigen Schritten nicht durch die Interaktionsliste abgedeckt wurden.\\
Nehmen wir an, dass die Partikel in etwa gleichverteilt sind, so erhalten wir aufgrund unserer Wahl der maximalen Verfeinerungsebene, dass die Anzahl der Partikel pro Box auf feinster Ebene durch eine Konstante beschränkt werden kann.\\
Da die Anzahl der Nachbarn einer Box ebenfalls durch eine Konstante beschränkt ist, sind die Interaktionen, welche pro Partikel noch berechnet werden müssen ebenfalls durch eine Konstante beschränkt.
Somit erhalten wir in diesem Schritt einen Aufwand in $\O(N)$.

\newpage
\textbf{Zusammenfassung und Fehlerabschätzung}\\
Als gesamten Rechenaufwand erhalten wir letztlich als Summe der Rechen- aufwände der einzelnen Schritte einen Aufwand in $\O(N\cdot \nu^5)$.
Dieser Aufwand kann jedoch noch reduziert werden, indem man die Anzahl der Auswertungen der sphärischen Harmonischen reduziert.\\
Es gibt zwei Schritte, in welchen $\nu^5$ in der Abschätzung vorkommt.
Der erste davon ist der Schritt, in welchem man die Multipolentwicklungen für jedes Element aus der Interaktionsliste einer Box in eine lokale Entwicklung umwandelt.\\
Dabei müssen $\O(\nu^4)$ Summanden berechnet werden, in welchen jeweils noch eine Auswertung einer sphärischen Harmonischen benötigt wird, welche ebenfalls einen Rechenaufwand in $\O(\nu)$ hat. \\
Wenn man jedoch genau betrachtet, welche sphärischen Harmonischen mit welchen Argumenten auszuwerten sind, so kann man sehen,
dass es für die Winkel, welche als Argumente auftreten, nur endlich viele verschiedene Mög- lichkeiten gibt. \\
Dies liegt daran, dass die Koordinaten, welche dort verwendet werden, nur von den relativen Positionen der Mittelpunkte der Boxen aus der Interaktionsliste abhängen. \\
Die Verfeinerungsebene, auf welcher man sich befindet, spielt nur für den Radius der relativen sphärischen Koordinaten eine Rolle, nicht aber für die vorkommenden Winkel. \\
Es ist also möglich sämtliche in Schritt 4 vorkommenden Auswertungen der sphärischen Harmonischen bereits vorher zu erledigen und die Ergebnisse abzuspeichern. \\
Der Aufwand dafür liegt in $\O(\nu^3)$, da es für die Indizes $\O(\nu^2)$ Möglichkeiten gibt, und die Auswertung einer sphärischen Harmonischen einen Aufwand in $\O(\nu)$ aufweist.
Die möglichen Winkel, sind jedoch durch eine Konstante zu beschränken, da es immer gleich viele Möglichkeiten gibt, wie eine Box relativ zu ihrem Vater liegen kann, und die daraus resultierende Interaktionsliste ebenfalls eine endliche Größe hat, welche durch eine Konstante beschränkt werden kann. \\
Diese Optimierung kann selbstverständlich auch in den Schritten 2 und 5 angewendet werden, um dort die Berechnung der Koeffizienten für die Translationen zu beschleunigen. \\
Insgesamt ergibt sich mit dieser Optimierung dann ein Gesamtaufwand in $\O(N\cdot \nu^4)$.\\
Der zweite Schritt, in welchem $\nu$ mit $5$ als Exponenten erscheint, ist Schritt 5, in welchem die lokalen Entwicklungen verschoben werden. \\
Auch hier kann man die vorkommenden sphärischen Harmonischen vorher auswerten, da hier nur die Translationen von Vätern in ihre eigenen Söhne vorgenommen werden. \\
Analog ist dies selbstverständlich auch für Schritt $2$ möglich, jedoch bleibt dabei das Wachstumsverhalten des Rechenaufwands in $\O(N\cdot \nu^4)$.

Für den Algorithmus, wie er hier formuliert wurde, wurden in \cite{kwast} bereits die entsprechenden Abschätzungen in Kapitel 3.2.2 durchgeführt, sodass wir diese hier lediglich zitieren werden. \\
Im Laufe des Algorthmus gibt es zwei Fehlerquellen. Zum einen gibt es die Ungenauigkeit, welche durch das Abschneiden der Multipolentwicklungen eingeführt wird, und zum anderen wird beim Konvertieren in lokale Entwicklungen ein weiterer Fehler durch das Abschneiden der Reihen für die Koeffizienten eingeführt. \\
Die Translation der lokalen Entwicklung führt keinen weiteren Fehler ein. \\
Die Translation der Multipolentwicklung führt ebenfalls keine neuen Fehler ein, jedoch wird die genaue Abschätzung für die Multipolentwicklungen auf gröberer Ebene schlechter. Der Fehler bleibt jedoch in der selben Größenordnung bezüglich der Landau-Notation.
Für die Fehler der Multipolentwicklung und der Konvertierung gibt es folgende Abschätzungen.
\begin{equation}
  \begin{array}{l}
    err_{multipole} \leq C_1 \left(\frac{1}{\sqrt{3}}\right)^{\nu+1} \\
    err_{konvert} \leq C_2 \left(\frac{3}{4\sqrt{3}-3}\right)^{\nu+1}
  \end{array}
\end{equation}
Hierbei sind die Konstanten $C_1$ und $C_2$ von den Ladungen der Partikel abhängig, welche in den jeweiligen Boxen enthalten sind.\\
Der Gesamtfehler lässt sich einfach als Summe der beiden berechnen, sodass wir insgesamt erwarten können, dass der Fehler exponenziell besser wird, wenn wir die Ordnung $\nu$ der Entwicklungen linear erhöhen.


%% wir werden den alg nciht direkt so implementieren, wie er dort steht, sondern die kips bib nutzen.

