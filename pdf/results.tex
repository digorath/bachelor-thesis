\section{Numerische Ergebnisse und Ausblick}
In diesem Abschnitt werden wir kurz die Ergebnisse der Implementierung diskutieren.
Das Programm wurde auf einem Notebook mit einem Intel® Core™ i5-6300HQ Prozessor und 16GB DDR3 RAM.\\

Wir möchten das Laufzeit-, Speicher- und Approximationsverhalten unter zwei Aspekten betrachten.
Zum einen möchten wir betrachten, wie sich diese Werte verändern, wenn die Eingabegröße des Problems variiert,
zum anderen wie sie sich verhalten, wenn die Approximationsordnung angehoben wird.

Anschließend werden wir noch einen kurzen Ausblick darauf geben, wie man diese Implementierung weiter optimieren kann,
wenn man die genutzte Bibliothek weiter verändert.

\subsection{Verhalten des Algorithmus unter Veränderung der Größe des Problems}
Für das erste Experiment wurde eine konstante Verfeinerungstiefe von 8 und eine konstante Approximationsordnung von 6 gewählt.
Für die Zeit T$_\text{K}$ wurden die Zeiten für die Verteilung der Partikel auf die Cluster, das Erstellen der Clusterbasen und dem Aufstellen der $\h^2$-Matrix zu der für die Matrix-Vektor-Multiplikation benötigten Zeit addiert. \\
Der Speicherbedarf Mem$_\text{K}$ beinhaltet den Speicher für die Matrix, sowie für die Clusterbasen.
Der Fehler err$_\text{K}$ beschreibt den relativen Fehler von der Approximation gegenüber des Referenzvektors. \\
Der Referenzvektor wird unter Verwendung der Funktion \verb|fillN_potential-| \verb|matrix|, welche auch zum Befüllen der unzulässigen Blöcke genutzt wird, berechnet.\\
\begin{table}[t]
  \centering
  \begin{tabular}{c||c|c|c||c|c}
    N       & T$_\text{K}$     & Mem$_\text{K}$     & err$_\text{K} $    & T$_\text{ref}$   & Mem$_\text{ref}$ \\ \hline
    2000    &  2.67s & 596.8 MB & 4.310e-06 & 0.13s & 61.0 MB \\
    4000    &  2.95s & 661.4 MB & 3.294e-06 & 0.43s & 244.1 MB \\
    6000    &  3.39s & 768.7 MB & 3.793e-06 & 0.95s & 549.3 MB \\
    8000    &  3.75s & 917.6 MB & 5.730e-06 & 1.73s & 976.6 MB \\
    10000   &  3.97s & 1108.6 MB & 7.290e-06 & 2.83s & 1525.9 MB \\
    12000   &  4.64s & 1345.5 MB & 4.951e-06 & 3.86s & 2197.3 MB \\
    14000   &  5.29s & 1627.5 MB & 7.145e-06 & 5.28s & 2990.7 MB \\
    16000   &  6.04s & 1946.7 MB & 8.318e-06 & 6.73s & 3906.3 MB \\
    18000   &  6.73s & 2311.9 MB & 6.916e-06 & 8.51s & 4943.8 MB \\
    20000   &  7.67s & 2712.1 MB & 7.311e-06 & 10.73s & 6103.5 MB \\
    22000   &  8.37s & 3159.8 MB & 5.909e-06 & 12.95s & 7385.3 MB 
    \label{tabN}
  \end{tabular}
  \caption[Tabelle]{Verhalten unter Veränderung der Problemgröße}
\end{table}
\newpage
In Abbildung \ref{runN} erkennt man sehr gut, dass die $\h^2$-Matrizen für kleine Problemgrößen zunächst ineffizient sind, sie jedoch ein deutlich besseres Verhalten aufweisen, wenn die Größe des Problems wächst.\\
\begin{figure}
  \label{runN}
  \centering
	\includegraphics[width=0.7\linewidth]{Diagrams/datN.png}
	\caption{Graphische Darstellung der Laufzeit in Tabelle \ref{tabN}}
\end{figure}
Die schwankende Größe des Fehlers in Tabelle \ref{tabN} ist hierbei dadurch zu erklären, dass für die verschiedenen Werte für die Anzahl der Partikel unterschiedliche zufällige Anordnungen und Ladungen erzeugt werden. \\
Desweiteren ist hierbei zu beachten, dass die Blattmatrizen bei fester Verfeinerungstiefe mit steigender Anzahl ebenfalls größer werden.
Es ist daher ratsam die Verfeinerungstiefe an die Anzahl der Partikel anzupassen.
Wählt man z.B. bei einer Anzahl von 2000 Partikeln als Baumtiefe nur 4 oder 6, so erhält man die Werte:
\begin{center}
  \begin{tabular}{c||c|c|c||c|c}
    Verfeinerungstiefe & T$_\text{K}$     & Mem$_\text{K}$     & err$_\text{K} $    & T$_\text{ref}$   & Mem$_\text{ref}$ \\ \hline
    4 &  0.23s & 66.3 MB & 1.829e-15 & 0.12s & 61.0 MB \\
    6 &  0.80s & 103.2 MB & 1.156e-06 & 0.12s & 61.0 MB
  \end{tabular}
\end{center}
Nicht nur wird viel Speicherplatz eingespart, sondern auch Rechenaufwand. \\
Der bessere Approximationsfehler bei geringerer Baumtiefe ist hierbei dadurch zu erklären, dass es weniger Clusterpaare gibt, die zulässig sind, und somit mehr Teile direkt ausgerechnet werden.\\
Wählt man hingegen bei einer Anzahl von 22000 Partikeln die Baumtiefe als 7 oder 9, so erhält man:
\begin{center}
  \begin{tabular}{c||c|c|c||c|c}
    Verfeinerungstiefe & T$_\text{K}$     & Mem$_\text{K}$     & err$_\text{K} $    & T$_\text{ref}$   & Mem$_\text{ref}$ \\ \hline
    7 &   10.26s & 3954.6 MB & 5.569e-06 & 14.21s & 7385.3 MB \\
    9 &  15.57s & 3628.2 MB & 6.247e-06 & 14.35s & 7385.3 MB
  \end{tabular}
\end{center}
Es stellt sich also heraus, dass die Baumtiefe 8 in dem Fall in Hinblick auf Laufzeit und Speicherbedarf optimal ist.
Welche Baumtiefe optimal ist, hängt ebenfalls von der Approximationsordnung ab, da diese die Dimension der Transfer und Kopplungsmatrizen bestimmt.

\subsection{Verhalten des Algorithmus unter Veränderung der Approximationsordnung}

In diesem Abschnitt beschäftigen wir uns mit dem Verhalten der Laufzeit, des Speicherbedarfs und dem Approximationsfehler unter Veränderung der Approximationsordnung.\\
Die Anzahl der Partikel wurde hierfür auf 12000 festelegt, und der Algorithmus wird mit einer Baumtiefe von 8 aufgerufen.
% Die Referenzwerte werden immer auf dieselbe Weise berechnet, daher sind sie nicht in der Tabelle aufgeführt.
% Sie sind

\begin{table}[h]
  \centering
  \begin{tabular}{c||c|c|c}
    $\nu$       & T$_\text{K}$     & Mem$_\text{K}$     & err$_\text{K} $ \\ \hline
    3 & 2.02s & 825.5 MB & 3.238e-04 \\
    5 & 3.26s & 1079.1 MB & 1.874e-05 \\
    7 & 6.98s & 1752.3 MB & 1.477e-06 \\
    9 & 15.19s & 3163.2 MB & 1.363e-07 \\
    11 & 32.02s & 5721.2 MB & 1.444e-08 \\
    13 & 61.67s & 9926.0 MB & 1.708e-09
    \label{tabNu}
  \end{tabular}
  \caption[Tabelle]{Verhalten unter Veränderung der Approximationsordnung. \\ Referenzwerte: T$_\text{ref}$ = 4.15s, Mem$_\text{ref}$=2197.3 MB}
\end{table}
% \begin{table}
%   \begin{tabular}{c|c}
%     T$_\text{ref}$   & Mem$_\text{ref}$ \\ \hline
%     4.15s & 2197.3 MB
%   \end{tabular}
%   \caption{Referenzwerte für Tabelle \ref{tabNu}.}
% \end{table}
An der Tabelle \ref{tabNu} ist sehr gut ersichtlich, dass die Approximationsordnung wie erwartet stark in die Laufzeit und den benötigten Speicherplatz eingeht. \\
Desweiteren sieht man, dass der Approximationsfehler wie erwartet exponentiell kleiner wird. \\
Wie bereits erwähnt, ist die optimale Baumtiefe auch von der gewählten Approximationsordnung abhängig.
Wählt man für eine Approximationsordnung von 13 z.b. nur die Baumtiefe 5, so erhält man deutlich bessere Werte für die Laufzeit und den Speicheraufwand, und ebenso für den Approximationsfehler:
\begin{table}[h]
  \centering
  \begin{tabular}{c|c||c|c|c}
    Verfeinerungstiefe & $\nu$       & T$_\text{K}$     & Mem$_\text{K}$     & err$_\text{K} $ \\ \hline
    5 & 13 & 7.92s & 2318.0 MB & 2.272e-11
  \end{tabular}
\end{table}

\subsection{Ausblick}
Wie bereits in den vorigen Abschnitten aufgezeigt, weist die Implementierung mit KIPS deutliche Speicherineffizienzen auf, welche auch die Laufzeit des Algorithmus nennenswert beeinträchtigen, da Matrizen unnötig kopiert werden müssen.\\
Sollte eine Implementierung mit KIPS langfristig genutzt werden sollen, so ist es wichtig diese Problematik zu beheben. \\
Desweiteren wäre es für die Konstruktion und Wiedervervendbarkeit der Kopplungsmatrizen möglicherweise sinnvoll, wenn die Cluster immer simultan in allen Richtungen verfeinert werden,
um eine Implementierung zu ermög- lichen, welche die Kopplungsmatrizen ähnlich verwaltet, wie die Transfermatrizen bereits verwaltet werden.

Ein weiterer wichtiger Aspekt, der bisher vernachlässigt worden ist, ist die Wiedervervendbarkeit der Transfer- und Kopplungsmatrizen im Allgemeinen.
Die Transfer- und Kopplungsmatrizen sind ausschließlich von der geometrischen Anordnung der Cluster abhängig, nicht jedoch von der Anzahl oder den Positionen der Partikel innerhalb eines Clusters.\\
Es ist also möglich, lediglich die Blattmatrizen und die vollbesetzten Matrizen für die unzulässigen Clusterpaare zu ersetzen, wenn sich die Eingabe des Problems ändert. \\
Dies kann insbesondere interessant sein, wenn man z.B. ein Partikelsystem über eine Zeitspanne betrachten möchte.
