#include "spherical_harmonics.h"

#include <math.h>
#include <complex.h>
#include <stdio.h>
#include <stdlib.h>

real*** associatedLegendreCoefficients;

real** calculateLegendreCoeff(int maxDegree);
real evalFromCoeffs(real* coeffs, real point, int degree);
real* derive(real* coeffs, int degree, int order);

real at2(real y, real x) {
  if(x>0) {
    return atan(y/x);
  } else if (y>0) {
    return M_PI * 0.5 - atan(x/y);
  } else if (y < 0) {
    return -atan(x/y) - 0.5 * M_PI;
  } else if (x<0) {
    return atan(y/x) + M_PI;
  }
  return 0.0;
}

field powCN(field base, int exponent) {
  field res;
  if (exponent == 0) return 1.0;
  if (exponent < 0) return 1.0/powCN(base, -exponent);
  if (exponent % 2 == 0) {
    res = powCN(base, exponent/2);
    return res * res;
  } else {
    return base * powCN(base, exponent - 1);
  }
}

void transformToSpherical(const real* origin, const real* point, real* res) {
  real r;
  real theta;
  real phi;
  for (int k = 0; k<3; k++) res[k] = point[k] - origin[k];
  r = sqrt(res[0]*res[0] + res[1] * res[1] + res[2]*res[2]);
  theta = acos(res[2]/r);
  phi = at2(res[1],res[0]);
  res[0] = r;
  res[1] = theta;
  res[2] = phi;
}

void setupCoefficients(int maxDegree) {
  real** tmp = calculateLegendreCoeff(maxDegree);
  associatedLegendreCoefficients = allocmem(sizeof(real**) * (maxDegree + 1));
  for(int l = 0; l <= maxDegree; l++) {
    associatedLegendreCoefficients[l] = allocmem(sizeof(real*) * (l + 1));
    for(int m = 0; m <= l; m++) {
      associatedLegendreCoefficients[l][m] = derive(tmp[l], l, abs(m));
    }
  }
  for(int l = 0; l <= maxDegree; l++) freemem(tmp[l]);
  freemem(tmp);
}

void deleteCoefficients(int maxDegree) {
  for(int l = 0; l <= maxDegree; l++) {
    for(int m = 0; m <= l; m++) {
      freemem(associatedLegendreCoefficients[l][m]);
    }
    freemem(associatedLegendreCoefficients[l]);
  }
  freemem(associatedLegendreCoefficients);
}

real evalFromCoeffs(real* coeffs, real point, int degree) {
  real res = 0.0;
  for(int k = 0; k <= degree; k++) {
    res = res * point;
    res += coeffs[k];
  }

  return res;
}

real evalAssociatedLegendre(int l, int m, real x) {
  real res;
  real faclminusm = prodFromTo(1, l-m);
  real faclplusm = prodFromTo(1, l+m);
  real* coeffs = associatedLegendreCoefficients[l][m];
  if (m < 0) {
    res = evalFromCoeffs(coeffs, cos(x), l+m);
    res = res* faclplusm/faclminusm;
    res = res * powCN(sin(x),-m);
  } else {
    res = evalFromCoeffs(coeffs, cos(x), l-m);
    res = res * powCN(-1.0, m)  * powCN(sin(x),m);
  }
  return res;
}

// TODO add parameter optimized and optimized routine cexp(I m phi) -> powCN(phi,m))
complex double evalSpherical(int l, int m, real theta, real phi) {
  complex double res;
  real faclminusm = prodFromTo(1, l-abs(m));
  real faclplusm = prodFromTo(1, l+abs(m));
  real tmp = evalAssociatedLegendre(l, abs(m), theta);
  res = cexp(I * m * phi) *  sqrt(faclminusm/faclplusm) * tmp;
  return res;
}

real prodFromTo(int n, int m) {
  real res = 1.0;
  for(int k = n; k <= m; k++) res = res * (real) k;
  return res;
}

real* derive(real* coeffs, int degree, int order) {
  int newDegree = degree - order;
  real* newCoeffs;

  if (newDegree < 0) {
    newCoeffs = (real*) allocmem(sizeof(real));
    newCoeffs[0] = 0.0;
    return newCoeffs;
  } else {
    newCoeffs = (real*) allocmem(sizeof(real) * (newDegree + 1));
    for(int k = 0; k <= newDegree; k++) {
      newCoeffs[k] = prodFromTo(degree - k - order + 1, degree - k) * coeffs[k];
    }
  }
  return newCoeffs;
}

real** calculateLegendreCoeff(int maxDegree) {
  int k,l;
  real** coeffs = (real **) allocmem(sizeof(real *) * (maxDegree + 1));
  for(k=0; k<=maxDegree; k++) {
    coeffs[k] = (real*) allocmem(sizeof(real) * (k+1));
  }

  coeffs[0][0] = 1;
  coeffs[1][0] = 1;
  coeffs[1][1] = 0;
  
  for(k=1; k<maxDegree;k++) {
    coeffs[k+1][0] = (2.0 * k + 1.0)/(1.0 * k + 1.0) * coeffs[k][0];
    coeffs[k+1][1] = 0.0;
    for(l = 2; l <= k; l++) {
      coeffs[k+1][l] = (2.0 * k + 1.0)/(1.0 * k + 1.0) * coeffs[k][l] - (1.0 * k / (1.0 * k + 1.0))* coeffs[k-1][l-2];
    }
    coeffs[k+1][k+1] = -(1.0 * k / (1.0 * k + 1.0)) * coeffs[k-1][k-1];
  }

  return coeffs;
}

// int main(int argc, char **argv) {
//   int maxDeg = 10;
//   real ** coeffs = calculateLegendreCoeff(maxDeg);
//   real * newCoeffs;

//   setupCoefficients(maxDeg);

//   // printf("product: %f\n", prodFromTo(3,7));
//   // for(int k = 0; k <=maxDeg; k++) {
//   //   printf("coeffs[%d]: ", k);
//   //   for(int l = 0; l <=k; l++){
//   //     printf("%f ", coeffs[k][l]);
//   //   }
//   //   printf("\n");
//   // }
//   newCoeffs = derive(coeffs[5], 5, 2);
//   printf("newCoeffs: ");
//   for(int l = 0; l <=3; l++){
//     printf("%f ", newCoeffs[l]);
//   }
//   printf("\n");
//   newCoeffs = associatedLegendreCoefficients[5][2];
//   printf("associatedLegendreCoefficients: ");
//   for(int l = 0; l <=3; l++){
//     printf("%f ", newCoeffs[l]);
//   }
//   printf("\n");

//   // 0.258707 + -0.402912 * I
//   complex double res = evalSpherical(5,-2,0.5,0.5);
//   real re = creal(res);
//   real im = cimag(res);
//   printf("%f + %f * I\n", re, im);

//   // 0.258707 + 0.402912 * I
//   res = evalSpherical(5,2,0.5,0.5);
//   re = creal(res);
//   im = cimag(res);
//   printf("%f + %f * I\n", re, im);  

//   real* orig = allocmem(sizeof(real)*3);
//   orig[0] = -1.0;
//   orig[1] = 1.0;
//   orig[2] = 0.5;

//   real* p = allocmem(sizeof(real) * 3);
//   p[0] = 0.5;
//   p[1] = -2.3;
//   p[2] = -1.7;

//   real* sph = allocmem(3*sizeof(real));
//   transformToSpherical(orig, p, sph);
//   printf("(1.5,-3.3, -2.2) = (%f,%f,%f) \n", sph[0] * sin(sph[1]) * cos(sph[2]), sph[0] * sin(sph[1]) * sin(sph[2]), sph[0] * cos(sph[1]));
// }
